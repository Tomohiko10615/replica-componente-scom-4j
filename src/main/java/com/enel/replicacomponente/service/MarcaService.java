package com.enel.replicacomponente.service;

import java.io.IOException;
import java.util.List;

import com.enel.replicacomponente.entity.sc4j.MedMarcaSC4J;
import com.enel.replicacomponente.entity.scom.MedMarcaSCOM;

public interface MarcaService {

	List<MedMarcaSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta) throws IOException;

	void verificarExistenciaEnSC4J(Long id) throws IOException;

	void replicarEnSC4J(MedMarcaSC4J registroReplicado) throws IOException;

}
