package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedMarcaSC4J;
import com.enel.replicacomponente.entity.scom.MedMarcaSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedMarcaSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedMarcaSCOMRepository;
import com.enel.replicacomponente.service.MarcaService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MarcaServiceImpl implements MarcaService {

	@Autowired
	private MedMarcaSC4JRepository sc4jDao;
	@Autowired
	private MedMarcaSCOMRepository scomDao;
	
	private String mensajeLog = "";
	
	public List<MedMarcaSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta) throws IOException {
		List<MedMarcaSCOM> medMarcaList = scomDao.obtenerUltimosRegistros(fechaDesde, fechaHasta);
		if (medMarcaList.isEmpty()) {
			mensajeLog = "No se encontraron registros en med_marca para procesar.";
		} else {
			mensajeLog = "Se encontraron "
					.concat(String.valueOf(medMarcaList.size()))
					.concat(" registro(s) en med_marca para procesar.");
		}
		LogWriter.escribirLog(mensajeLog);
		return medMarcaList;
	}

	public void verificarExistenciaEnSC4J(Long id) throws IOException {
		String valorAnterior = "";
		MedMarcaSC4J medMarcaSC4J = sc4jDao.findById(id).orElse(null);
		if (medMarcaSC4J == null) {
			mensajeLog = 
					"La marca no existe en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "La marca existe en el SC4J. Se actualizará el registro.";
			valorAnterior = medMarcaSC4J.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}

	public void replicarEnSC4J(MedMarcaSC4J medMarcaSC4J) throws IOException {
		String valorNuevo = "";
		try {
			MedMarcaSC4J registroReplicado = sc4jDao.save(medMarcaSC4J);
			mensajeLog = "Se actualizó la marca en el SC4J.";
			valorNuevo = registroReplicado.toString();
			LogWriter.escribirLog(mensajeLog);
			LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó la marca en el SC4J: {}.", medMarcaSC4J);
		}
		
	}

}
