package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedCalibracionSC4J;
import com.enel.replicacomponente.entity.scom.MedCalibracionSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedCalibracionSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedCalibracionSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("calibracionService4J")
public class CalibracionService4J {

	@Autowired
	private MedCalibracionSCOMRepository scomDao;
	@Autowired
	private MedCalibracionSC4JRepository sc4jDao;
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		List<MedCalibracionSC4J> registrosPorReplicar = sc4jDao.findByIdComponente(idComponente);
		if (!registrosPorReplicar.isEmpty()) {
			List<MedCalibracionSCOM> registrosReplicados = replicarRegistros(registrosPorReplicar);
			replicarEnSCOM(registrosReplicados);
		}
	}
	
	private List<MedCalibracionSCOM> replicarRegistros(List<MedCalibracionSC4J> registrosPorReplicar) {
		List<MedCalibracionSCOM> nuevoMedCalibracionSCOMList = new ArrayList<>();
		MedCalibracionSCOM nuevoMedCalibracionSCOM;
		for (MedCalibracionSC4J registroPorReplicar : registrosPorReplicar) {
			nuevoMedCalibracionSCOM = new MedCalibracionSCOM();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedCalibracionSCOM);
			nuevoMedCalibracionSCOMList.add(nuevoMedCalibracionSCOM);
		}
		return nuevoMedCalibracionSCOMList;
	}

	private void replicarEnSCOM(List<MedCalibracionSCOM> medCalibracionSCOMList) throws IOException {
		String mensajeLog = "";
		String valorNuevo = "";
		MedCalibracionSCOM registroReplicado;
		for (MedCalibracionSCOM medCalibracionSCOM : medCalibracionSCOMList) {
			try {
				Integer count = scomDao.countByIdCalibracion(medCalibracionSCOM.getIdCalibracion());
				if (count == 0) {
					registroReplicado = scomDao.save(medCalibracionSCOM);
					mensajeLog = "Se actualizó la calibración en el SCOM.";
					valorNuevo = registroReplicado.toString();
					LogWriter.escribirLog(mensajeLog);
					LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				}
				LogWriter.escribirLog("Ya existe: ".concat(medCalibracionSCOM.toString()));
			} catch (Exception e) {
				log.error("No se actualizó la calibración en el SCOM: {}.", medCalibracionSCOM);
			}
		}
	}
}
