package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.dto.MedidaModeloFactorDTO;
import com.enel.replicacomponente.entity.sc4j.MedFacMedModSC4J;
import com.enel.replicacomponente.entity.sc4j.MedMedidaModeloSC4J;
import com.enel.replicacomponente.entity.scom.MedFacMedModSCOM;
import com.enel.replicacomponente.entity.scom.MedMedidaModeloSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedFacMedModSC4JRepository;
import com.enel.replicacomponente.repository.sc4j.MedMedidaModeloSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedFacMedModSCOMRepository;
import com.enel.replicacomponente.repository.scom.MedMedidaModeloSCOMRepository;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
@Qualifier("medidaModeloService")
public class MedidaModeloService {

	@Autowired
	private MedMedidaModeloSCOMRepository scomDao;
	@Autowired
	private MedFacMedModSCOMRepository scomDaoFacMedMod;
	@Autowired
	private MedMedidaModeloSC4JRepository sc4jDao;
	@Autowired
	private MedFacMedModSC4JRepository sc4jDaoFacMedMod;
	
	private String mensajeLog = "";
	private Set<Long> idMedidaModeloSet = new HashSet<>();

	public List<Object[]> obtenerRegistros() throws IOException {
		List<Object[]> medModeloList = scomDao.obtenerRegistros();
		if (medModeloList.isEmpty()) {
			mensajeLog = "No se encontraron registros en med_modelo para procesar.";
		} else {
			mensajeLog = "Se encontraron "
					.concat(String.valueOf(medModeloList.size()))
					.concat(" registro(s) en med_modelo para procesar.");
		}
		LogWriter.escribirLog(mensajeLog);
		return medModeloList;
	}
	
	public List<MedMedidaModeloSCOM> obtenerUltimosRegistros(Long idMax) throws IOException {
		List<MedMedidaModeloSCOM> medModeloList = scomDao.obtenerUltimosRegistros(idMax);
		if (medModeloList.isEmpty()) {
			mensajeLog = "No se encontraron registros en med_modelo para procesar.";
		} else {
			mensajeLog = "Se encontraron "
					.concat(String.valueOf(medModeloList.size()))
					.concat(" registro(s) en med_modelo para procesar.");
		}
		LogWriter.escribirLog(mensajeLog);
		return medModeloList;
	}
	
	public Long obtenerUltimosRegistro() throws IOException {
		Long idMax = sc4jDao.obtenerIdMax();
		LogWriter.escribirLog("Med_medida_medidor-Id_maximo: ".concat(String.valueOf(idMax)));
		return idMax;
	}
	
	public Long obtenerTotal4J() throws IOException {
		Long idMax = sc4jDao.obtenerTotalFac();
		LogWriter.escribirLog("Total de registros en med_fac_med_mod 4J: ".concat(String.valueOf(idMax)));
		return idMax;
	}
	
	public Long obtenerTotalScom() throws IOException {
		Long idMax = scomDao.obtenerTotalFac();
		LogWriter.escribirLog("Total de registros en med_fac_med_mod SCOM: ".concat(String.valueOf(idMax)));
		return idMax;
	}
	
	public void replicarRegistroAsociado(Long IdModelo) throws IOException {
		List<MedMedidaModeloSCOM> registrosPorReplicar =scomDao.findByIdModelo(IdModelo);
		if (!registrosPorReplicar.isEmpty()) {
			verificarExistenciaEnSC4J(IdModelo);
			List<MedMedidaModeloSC4J> registrosReplicados = replicarRegistros(registrosPorReplicar);
			replicarEnSC4J(registrosReplicados);
			List<MedFacMedModSCOM> registrosPorReplicarFacMedMod =scomDaoFacMedMod.findByIdMedidaModelo(IdModelo);
			List<MedFacMedModSC4J> registrosReplicadosFacMedMod = replicarRegistrosFacMedMod(registrosPorReplicarFacMedMod);
			replicarEnSC4JFacMedMod(registrosReplicadosFacMedMod);
		}
	}
	
	private void verificarExistenciaEnSC4J(Long IdModelo) throws IOException {
		String valorAnterior = "";
		List<MedMedidaModeloSC4J> medMedidaModeloSC4JList = sc4jDao.findByIdModelo(IdModelo);
		if (medMedidaModeloSC4JList.isEmpty()) {
			mensajeLog = 
					"Las medidas del modelo del medidor no existen en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "Las medidas del modelo del medidor existen en el SC4J. Se actualizará el registro.";
			valorAnterior = medMedidaModeloSC4JList.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}
	
	public List<MedMedidaModeloSC4J> replicarRegistros(List<MedMedidaModeloSCOM> registrosPorReplicar) {
		List<MedMedidaModeloSC4J> nuevoMedMedidaModeloSC4JList = new ArrayList<>();
		MedMedidaModeloSC4J nuevoMedMedidaModeloSC4J;
		for (MedMedidaModeloSCOM registroPorReplicar : registrosPorReplicar) {
			nuevoMedMedidaModeloSC4J = new MedMedidaModeloSC4J();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedMedidaModeloSC4J);
			nuevoMedMedidaModeloSC4J.setIdMedidaModelo(registroPorReplicar.getId());
			nuevoMedMedidaModeloSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
			nuevoMedMedidaModeloSC4JList.add(nuevoMedMedidaModeloSC4J);
		}
		return nuevoMedMedidaModeloSC4JList;
	}

	public void replicarEnSC4J(List<MedMedidaModeloSC4J> medMedidaModeloSC4JList) throws IOException {
		String valorNuevo = "";
		MedMedidaModeloSC4J registroReplicado;
		for (MedMedidaModeloSC4J medMedidaModeloSC4J : medMedidaModeloSC4JList) {
			boolean valido = validarMedidaModelo(medMedidaModeloSC4J.getIdModelo(), medMedidaModeloSC4J.getIdMedida());
			if (valido) {
				try {
					registroReplicado = sc4jDao.save(medMedidaModeloSC4J);
					mensajeLog = "Se actualizó la medida del modelo del medidor en el SC4J.";
					valorNuevo = registroReplicado.toString();
					LogWriter.escribirLog(mensajeLog);
					LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				} catch (Exception e) {
					log.error("Se actualizó la medida del modelo del medidor en el SC4J: {}.", medMedidaModeloSC4J);
				}
				
			} else {
				log.info("Registro inválido, ya existe la id_medida y id_modelo en el SC4J");
			}
		}
	}

	private boolean validarMedidaModelo(Long idModelo, Long idMedida) {
		Set<Long> newIdMedidaModeloSet = sc4jDao.verificarExistenciaMedidaModelo(idModelo, idMedida);
		idMedidaModeloSet.addAll(newIdMedidaModeloSet);
		return newIdMedidaModeloSet.isEmpty();
	}

	public List<MedFacMedModSC4J> replicarRegistrosFacMedModObj(List<Object[]> registrosPorReplicar) {
		List<MedFacMedModSC4J> nuevoMedMedidaModeloSC4JList = new ArrayList<>();
		MedFacMedModSC4J nuevoMedMedidaModeloSC4J;
		for (Object[] registroPorReplicar : registrosPorReplicar) {
			nuevoMedMedidaModeloSC4J = new MedFacMedModSC4J();
			nuevoMedMedidaModeloSC4J.setIdMedidaModelo(Long.valueOf(registroPorReplicar[0].toString()));
			nuevoMedMedidaModeloSC4J.setIdFactor(Long.valueOf(registroPorReplicar[1].toString()));
			nuevoMedMedidaModeloSC4JList.add(nuevoMedMedidaModeloSC4J);
		}
		return nuevoMedMedidaModeloSC4JList;
	}

	public List<MedFacMedModSC4J> replicarRegistrosFacMedMod(List<MedFacMedModSCOM> registrosPorReplicar) {
		List<MedFacMedModSC4J> nuevoMedMedidaModeloSC4JList = new ArrayList<>();
		MedFacMedModSC4J nuevoMedMedidaModeloSC4J;
		for (MedFacMedModSCOM registroPorReplicar : registrosPorReplicar) {
			nuevoMedMedidaModeloSC4J = new MedFacMedModSC4J();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedMedidaModeloSC4J);
			nuevoMedMedidaModeloSC4JList.add(nuevoMedMedidaModeloSC4J);
		}
		return nuevoMedMedidaModeloSC4JList;
	}

	public void replicarEnSC4JFacMedMod(List<MedFacMedModSC4J> medMedidaModeloSC4JList) throws IOException {
		String valorNuevo = "";
		MedFacMedModSC4J registroReplicado;
		for (MedFacMedModSC4J medMedidaModeloSC4J : medMedidaModeloSC4JList) {
			try {
				registroReplicado = sc4jDaoFacMedMod.save(medMedidaModeloSC4J);
				mensajeLog = "Se actualizó la med_fac_med_mod del medidor en el SC4J.";
				valorNuevo = registroReplicado.toString();
				LogWriter.escribirLog(mensajeLog);
				LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
			} catch (Exception e) {
				log.error("No se actualizó la med_fac_med_mod del medidor en el SC4J: {}.", medMedidaModeloSC4J);
			}
		}
	}
	
	public Set<Long> getIdMedidaModeloSet() {
		return idMedidaModeloSet;
	}

	public List<MedFacMedModSCOM> obtenerRegistros(List<MedMedidaModeloSC4J> medMedidaModeloSC4JList) {
		List<MedFacMedModSCOM> medFacMedModSCOMList = new ArrayList<>();
		List<MedFacMedModSCOM> medFacMedModSCOMTempList = null;
		for (MedMedidaModeloSC4J medMedidaModeloSC4J : medMedidaModeloSC4JList) {
			medFacMedModSCOMTempList = scomDaoFacMedMod.findByIdMedidaModelo(medMedidaModeloSC4J.getIdMedidaModelo());
			if (!medFacMedModSCOMTempList.isEmpty()) {
				medFacMedModSCOMList.addAll(medFacMedModSCOMTempList);
			}
		}
		return medFacMedModSCOMList;
	}

	public List<MedFacMedModSCOM> obtenerTodosRegistros() {
		return scomDaoFacMedMod.findAll();
	}

	public List<MedidaModeloFactorDTO> obtenerMedidaModeloFactor() {
		return scomDaoFacMedMod.obtenerMedidaModeloFactor();
	}

	public Integer obtenerCountFactorSC4J(Long idMedidaModelo) {
		Integer count = sc4jDaoFacMedMod.obtenerCountFactor(idMedidaModelo);
		if (count == null) {
			count = 0;
		}
		return count;
	}

	public List<MedFacMedModSCOM> obtenerRegistrosAdicionales(List<Long> medModIdList) {
		return scomDaoFacMedMod.findByIdMedidaModeloIn(medModIdList);
	}

}
