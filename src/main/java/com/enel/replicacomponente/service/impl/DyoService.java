package com.enel.replicacomponente.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.DyoObjectSC4J;
import com.enel.replicacomponente.entity.scom.DyoObjectSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.DyoObjectSC4JRepository;
import com.enel.replicacomponente.repository.scom.DyoObjectSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("dyoService")
public class DyoService {

	@Autowired
	private DyoObjectSCOMRepository scomDao;
	@Autowired
	private DyoObjectSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(Long idObject) throws IOException {
		DyoObjectSCOM registroPorReplicar = obtenerRegistro(idObject);
		verificarExistenciaEnSC4J(idObject);
		DyoObjectSC4J registroReplicado = replicarRegistro(registroPorReplicar);
		replicarEnSC4J(registroReplicado);
	}

	private DyoObjectSCOM obtenerRegistro(Long idObject) throws IOException {
		DyoObjectSCOM dyoObjectSCOM = scomDao.findById(idObject).orElse(null);
		if (dyoObjectSCOM == null) {
			log.error("Error al obtener el objeto dinámico");
			mensajeLog = "El objeto dinámico no existe. Se ejecutó el rollback.";
			LogWriter.escribirLog(mensajeLog);
			throw new EmptyResultDataAccessException(1);
		}
		return dyoObjectSCOM;
	}
	
	private void verificarExistenciaEnSC4J(Long idObject) throws IOException {
		String valorAnterior = "";
		DyoObjectSC4J dyoObjectSC4J = sc4jDao.findById(idObject).orElse(null);
		if (dyoObjectSC4J == null) {
			mensajeLog = 
					"El objeto dinámico no existe en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "El objeto dinámico existe en el SC4J. Se actualizará el registro.";
			valorAnterior = dyoObjectSC4J.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}
	
	private DyoObjectSC4J replicarRegistro(DyoObjectSCOM dyoObjectSCOM) {
		DyoObjectSC4J nuevoDyoObjectSC4J = new DyoObjectSC4J();
		nuevoDyoObjectSC4J.setIdObject(dyoObjectSCOM.getId());
		nuevoDyoObjectSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
		return nuevoDyoObjectSC4J;
	}

	private void replicarEnSC4J(DyoObjectSC4J dyoObjectSC4J) {
		String valorNuevo = "";
		try {
			DyoObjectSC4J registroReplicado = sc4jDao.save(dyoObjectSC4J);
			mensajeLog = "Se actualizó el objeto dinámico en el SC4J.";
			valorNuevo = registroReplicado.toString();
			LogWriter.escribirLog(mensajeLog);
			LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó el objeto dinámico en el SC4J: {}.", dyoObjectSC4J);
		}
		
	}

}
