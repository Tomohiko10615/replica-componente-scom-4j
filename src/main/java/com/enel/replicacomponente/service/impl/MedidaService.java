package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedMedidaMedidorSC4J;
import com.enel.replicacomponente.entity.scom.MedMedidaMedidorSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedMedidaMedidorSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedMedidaMedidorSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("medidaService")
public class MedidaService {

	@Autowired
	private MedMedidaMedidorSCOMRepository scomDao;
	@Autowired
	private MedMedidaMedidorSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		List<MedMedidaMedidorSCOM> registrosPorReplicar =scomDao.findByIdComponente(idComponente);
		int countRegistrosEnSC4J = sc4jDao.countByIdComponente(idComponente);
		// if (registrosPorReplicar.size() > countRegistrosEnSC4J) {
		verificarExistenciaEnSC4J(idComponente);
		// eliminar las medidas del medidor del componente respectivo
		sc4jDao.deleteByIdComponente(idComponente);
		// obtiene las medidas a replicar de scom y las guarda en una varible de sc4j
		List<MedMedidaMedidorSC4J> registrosReplicados = replicarRegistros(registrosPorReplicar);
		// guarda la informacion en sc4j
		replicarEnSC4J(registrosReplicados);
		// }
	}
	
	private void verificarExistenciaEnSC4J(Long idComponente) throws IOException {
		String valorAnterior = "";
		List<MedMedidaMedidorSC4J> medMedidaMedidorSC4JList = sc4jDao.findByIdComponente(idComponente);
		if (medMedidaMedidorSC4JList.isEmpty()) {
			mensajeLog = 
					"Las medidas del medidor no existen en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "Las medidas del medidor existen en el SC4J. Se actualizará el registro.";
			valorAnterior = medMedidaMedidorSC4JList.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}
	
	private List<MedMedidaMedidorSC4J> replicarRegistros(List<MedMedidaMedidorSCOM> registrosPorReplicar) {
		List<MedMedidaMedidorSC4J> nuevoMedMedidaMedidorSC4JList = new ArrayList<>();
		MedMedidaMedidorSC4J nuevoMedMedidaMedidorSC4J;
		for (MedMedidaMedidorSCOM registroPorReplicar : registrosPorReplicar) {
			nuevoMedMedidaMedidorSC4J = new MedMedidaMedidorSC4J();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedMedidaMedidorSC4J);
			nuevoMedMedidaMedidorSC4J.setIdMedidaMedidor(registroPorReplicar.getId());
			nuevoMedMedidaMedidorSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
			nuevoMedMedidaMedidorSC4JList.add(nuevoMedMedidaMedidorSC4J);
		}
		return nuevoMedMedidaMedidorSC4JList;
	}

	private void replicarEnSC4J(List<MedMedidaMedidorSC4J> medMedidaMedidorSC4JList) throws IOException {
		String valorNuevo = "";
		MedMedidaMedidorSC4J registroReplicado;
		for (MedMedidaMedidorSC4J medMedidaMedidorSC4J : medMedidaMedidorSC4JList) {
			try {
				registroReplicado = sc4jDao.save(medMedidaMedidorSC4J);
				mensajeLog = "Se actualizó la medida del medidor en el SC4J.";
				valorNuevo = registroReplicado.toString();
				LogWriter.escribirLog(mensajeLog);
				LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
			} catch (Exception e) {
				log.error("No se actualizó la medida del medidor en el SC4J: {}.", medMedidaMedidorSC4J);
			}
			
		}
	}
}
