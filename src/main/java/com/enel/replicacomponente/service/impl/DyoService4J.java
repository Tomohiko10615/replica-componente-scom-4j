package com.enel.replicacomponente.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.DyoObjectSC4J;
import com.enel.replicacomponente.entity.scom.DyoObjectSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.DyoObjectSC4JRepository;
import com.enel.replicacomponente.repository.scom.DyoObjectSCOMRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("dyoService4J")
public class DyoService4J {

	@Autowired
	private DyoObjectSCOMRepository scomDao;
	@Autowired
	private DyoObjectSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(Long idObject) throws IOException {
		DyoObjectSC4J registroPorReplicar = obtenerRegistro(idObject);
		DyoObjectSCOM registroReplicado = replicarRegistro(registroPorReplicar);
		replicarEnSCOM(registroReplicado);
	}

	private DyoObjectSC4J obtenerRegistro(Long idObject) throws IOException {
		DyoObjectSC4J dyoObjectSC4J = sc4jDao.findById(idObject).orElse(null);
		if (dyoObjectSC4J == null) {
			log.error("Error al obtener el objeto dinámico");
			mensajeLog = "El objeto dinámico no existe. Se ejecutó el rollback.";
			LogWriter.escribirLog(mensajeLog);
			throw new EmptyResultDataAccessException(1);
		}
		return dyoObjectSC4J;
	}
	
	private DyoObjectSCOM replicarRegistro(DyoObjectSC4J dyoObjectSC4J) {
		DyoObjectSCOM nuevoDyoObjectSCOM = new DyoObjectSCOM();
		nuevoDyoObjectSCOM.setId(dyoObjectSC4J.getIdObject());
		return nuevoDyoObjectSCOM;
	}

	private void replicarEnSCOM(DyoObjectSCOM dyoObjectSCOM) throws IOException {
		String valorNuevo = "";
		try {
			DyoObjectSCOM registroReplicado = scomDao.save(dyoObjectSCOM);
			mensajeLog = "Se actualizó el objeto dinámico en el SCOM.";
			valorNuevo = registroReplicado.toString();
			LogWriter.escribirLog(mensajeLog);
			LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó el objeto dinámico en el SCOM {}.", dyoObjectSCOM);
		}
	}
}
