package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedModeloSC4J;
import com.enel.replicacomponente.entity.scom.MedModeloSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedModeloSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedModeloSCOMRepository;
import com.enel.replicacomponente.service.ModeloService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ModeloServiceImpl implements ModeloService {
	
	@Autowired
	private MedModeloSCOMRepository scomDao;
	@Autowired
	private MedModeloSC4JRepository sc4jDao;
	
	private String mensajeLog = "";

	@Override
	public List<MedModeloSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta) throws IOException {
		List<MedModeloSCOM> medModeloList = scomDao.obtenerUltimosRegistros(fechaDesde, fechaHasta);
		if (medModeloList.isEmpty()) {
			mensajeLog = "No se encontraron registros en med_modelo para procesar.";
		} else {
			mensajeLog = "Se encontraron "
					.concat(String.valueOf(medModeloList.size()))
					.concat(" registro(s) en med_modelo para procesar.");
		}
		LogWriter.escribirLog(mensajeLog);
		return medModeloList;
	}

	@Override
	public void verificarExistenciaEnSC4J(Long id) throws IOException {
		String valorAnterior = "";
		MedModeloSC4J medModeloSC4J = sc4jDao.findById(id).orElse(null);
		if (medModeloSC4J == null) {
			mensajeLog = 
					"El modelo no existe en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "El modelo existe en el SC4J. Se actualizará el registro.";
			valorAnterior = medModeloSC4J.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}

	@Override
	public void replicarEnSC4J(MedModeloSC4J medModeloSC4J) throws IOException {
		String valorNuevo = "";
		try {
			MedModeloSC4J registroReplicado = sc4jDao.save(medModeloSC4J);
			mensajeLog = "Se actualizó el modelo en el SC4J.";
			valorNuevo = registroReplicado.toString();
			LogWriter.escribirLog(mensajeLog);
			LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó el modelo en el SC4J.", medModeloSC4J);
		}
	}
}
