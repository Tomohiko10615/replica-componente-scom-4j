package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.FwkAuditeventSC4J;
import com.enel.replicacomponente.entity.scom.FwkAuditeventSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.FwkAuditeventSC4JRepository;
import com.enel.replicacomponente.repository.scom.FwkAuditeventSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("auditeventService4J")
public class AuditeventService4J {

	@Autowired
	private FwkAuditeventSCOMRepository scomDao;
	@Autowired
	private FwkAuditeventSC4JRepository sc4jDao;
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		List<FwkAuditeventSC4J> registrosPorReplicar = sc4jDao.findByIdFk(idComponente);
		if (!registrosPorReplicar.isEmpty()) {
			List<FwkAuditeventSCOM> registrosReplicado = replicarRegistros(registrosPorReplicar);
			replicarEnSCOM(registrosReplicado);
		}
	}
	
	private List<FwkAuditeventSCOM> replicarRegistros(List<FwkAuditeventSC4J> registrosPorReplicar) {
		List<FwkAuditeventSCOM> nuevoFwkAuditeventSCOMList = new ArrayList<>();
		FwkAuditeventSCOM nuevoFwkAuditeventSCOM;
		for (FwkAuditeventSC4J registroPorReplicar : registrosPorReplicar) {
			nuevoFwkAuditeventSCOM = new FwkAuditeventSCOM();
			BeanUtils.copyProperties(registroPorReplicar, nuevoFwkAuditeventSCOM);
			nuevoFwkAuditeventSCOM.setId(registroPorReplicar.getIdAuditevent());
			nuevoFwkAuditeventSCOMList.add(nuevoFwkAuditeventSCOM);
		}
		return nuevoFwkAuditeventSCOMList;
	}

	private void replicarEnSCOM(List<FwkAuditeventSCOM> fwkAuditeventSCOMList) throws IOException {
		String mensajeLog = "";
		String valorNuevo = "";
		FwkAuditeventSCOM registroReplicado;
		for (FwkAuditeventSCOM fwkAuditeventSCOM : fwkAuditeventSCOMList) {
			try {
				Integer count = scomDao.countById(fwkAuditeventSCOM.getId());
				if (count == 0) {
					registroReplicado = scomDao.save(fwkAuditeventSCOM);
					mensajeLog = "Se actualizó la auditoría en el SCOM.";
					valorNuevo = registroReplicado.toString();
					LogWriter.escribirLog(mensajeLog);
					LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				}
				LogWriter.escribirLog("Ya existe: ".concat(fwkAuditeventSCOM.toString()));
			} catch (Exception e) {
				log.error("Se actualizó la auditoría en el SCOM: {}", fwkAuditeventSCOM);
			}
		}
	}
}
