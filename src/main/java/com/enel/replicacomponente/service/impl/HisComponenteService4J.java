package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedComponenteSC4J;
import com.enel.replicacomponente.entity.sc4j.MedHisComponenteSC4J;
import com.enel.replicacomponente.entity.scom.MedHisComponenteSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedHisComponenteSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedHisComponenteSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("hisComponenteService4J")
public class HisComponenteService4J {

	@Autowired
	private MedHisComponenteSCOMRepository scomDao;
	@Autowired
	private MedHisComponenteSC4JRepository sc4jDao;
	
	public void replicarRegistroAsociado(List<MedComponenteSC4J> medComponenteSC4JList) throws IOException {
		List<MedHisComponenteSC4J> registrosPorReplicar = new ArrayList<>();
		for (MedComponenteSC4J medComponenteSC4J : medComponenteSC4JList) {
			registrosPorReplicar.addAll(sc4jDao.obtenerRegistros(medComponenteSC4J.getIdComponente()));
		}
		if (!registrosPorReplicar.isEmpty()) {
			List<MedHisComponenteSCOM> registrosReplicados = replicarRegistros(registrosPorReplicar);
			replicarEnSCOM(registrosReplicados);
		}
	}
	
	private List<MedHisComponenteSCOM> replicarRegistros(List<MedHisComponenteSC4J> registrosPorReplicar) {
		List<MedHisComponenteSCOM> nuevoMedHisComponenteSCOMList = new ArrayList<>();
		MedHisComponenteSCOM nuevoMedHisComponenteSCOM;
		for (MedHisComponenteSC4J registroPorReplicar : registrosPorReplicar) {
			nuevoMedHisComponenteSCOM = new MedHisComponenteSCOM();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedHisComponenteSCOM);
			nuevoMedHisComponenteSCOMList.add(nuevoMedHisComponenteSCOM);
		}
		return nuevoMedHisComponenteSCOMList;
	}

	private void replicarEnSCOM(List<MedHisComponenteSCOM> medHisComponenteSCOMList) throws IOException {
		String mensajeLog = "";
		String valorNuevo = "";
		MedHisComponenteSCOM registroReplicado;
		for (MedHisComponenteSCOM medHisComponenteSCOM : medHisComponenteSCOMList) {
			try {
				registroReplicado = scomDao.save(medHisComponenteSCOM);
				mensajeLog = "Se actualizó el historial del componente en el SCOM.";
				valorNuevo = registroReplicado.toString();
				LogWriter.escribirLog(mensajeLog);
				LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
			} catch (Exception e) {
				log.error("No se actualizó el historial del componente en el SCOM: {}.", medHisComponenteSCOM);
			}
		}
	}

	public List<Long> obtenerIdUltimosRegistros(String fechaDesde) {
		return scomDao.obtenerIdUltimosRegistros(fechaDesde);
	}

	/*
	public List<MedHisComponenteSC4J> obtenerHistorial(String fechaDesde) {
		return sc4jDao.obtenerHistorial(fechaDesde);
	}
	*/
}
