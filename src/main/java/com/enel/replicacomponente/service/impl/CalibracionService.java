package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedCalibracionSC4J;
import com.enel.replicacomponente.entity.scom.MedCalibracionSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedCalibracionSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedCalibracionSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("calibracionService")
public class CalibracionService {

	@Autowired
	private MedCalibracionSCOMRepository scomDao;
	@Autowired
	private MedCalibracionSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		List<MedCalibracionSCOM> registrosPorReplicar = scomDao.findByIdComponente(idComponente);
		if (!registrosPorReplicar.isEmpty()) {
			//verificarExistenciaEnSC4J(idComponente);
			List<MedCalibracionSC4J> registrosReplicados = replicarRegistros(registrosPorReplicar);
			replicarEnSC4J(registrosReplicados);
		}
	}
	
	private void verificarExistenciaEnSC4J(Long idComponente) throws IOException {
		String valorAnterior = "";
		List<MedCalibracionSC4J> medCalibracionSC4JList = sc4jDao.findByIdComponente(idComponente);
		if (medCalibracionSC4JList.isEmpty()) {
			mensajeLog = 
					"La calibracion no existe en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "La calibracion existe en el SC4J. Se actualizará el registro.";
			valorAnterior = medCalibracionSC4JList.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}
	
	private List<MedCalibracionSC4J> replicarRegistros(List<MedCalibracionSCOM> registrosPorReplicar) {
		List<MedCalibracionSC4J> nuevoMedCalibracionSC4JList = new ArrayList<>();
		MedCalibracionSC4J nuevoMedCalibracionSC4J;
		for (MedCalibracionSCOM registroPorReplicar : registrosPorReplicar) {
			nuevoMedCalibracionSC4J = new MedCalibracionSC4J();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedCalibracionSC4J);
			nuevoMedCalibracionSC4JList.add(nuevoMedCalibracionSC4J);
		}
		return nuevoMedCalibracionSC4JList;
	}

	private void replicarEnSC4J(List<MedCalibracionSC4J> medCalibracionSC4JList) throws IOException {
		String valorNuevo = "";
		MedCalibracionSC4J registroReplicado;
		for (MedCalibracionSC4J medCalibracionSC4J : medCalibracionSC4JList) {
			try {
				Integer count = sc4jDao.countByIdCalibracion(medCalibracionSC4J.getIdCalibracion());
				if (count == 0) {
					registroReplicado = sc4jDao.save(medCalibracionSC4J);
					mensajeLog = "Se actualizó la calibración en el SC4J.";
					valorNuevo = registroReplicado.toString();
					LogWriter.escribirLog(mensajeLog);
					LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				}
				LogWriter.escribirLog("Ya existe: ".concat(medCalibracionSC4J.toString()));
			} catch (Exception e) {
				log.error("No se actualizó la calibración en el SC4J: {}.", medCalibracionSC4J);
			}
		}
	}
}
