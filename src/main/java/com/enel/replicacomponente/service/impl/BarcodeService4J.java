package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedBarcodeSC4J;
import com.enel.replicacomponente.entity.scom.MedBarcodeSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedBarcodeSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedBarcodeSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("barcodeService4J")
public class BarcodeService4J {

	@Autowired
	private MedBarcodeSCOMRepository scomDao;
	@Autowired
	private MedBarcodeSC4JRepository sc4jDao;
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		Optional<MedBarcodeSC4J> registroPorReplicar = sc4jDao.findByIdComponente(idComponente);
		if (registroPorReplicar.isPresent()) {
			MedBarcodeSCOM registroReplicado = replicarRegistro(registroPorReplicar.get());
			replicarEnSCOM(registroReplicado);
		}
	}
	
	private MedBarcodeSCOM replicarRegistro(MedBarcodeSC4J medBarcodeSC4J) {
		MedBarcodeSCOM nuevoMedBarcodeSCOM = new MedBarcodeSCOM();
		BeanUtils.copyProperties(medBarcodeSC4J, nuevoMedBarcodeSCOM);
		return nuevoMedBarcodeSCOM;
	}

	private void replicarEnSCOM(MedBarcodeSCOM medBarcodeSCOM) {
		String mensajeLog = "";
		String valorNuevo = "";
		try {
			Integer count = scomDao.countByIdBarcode(medBarcodeSCOM.getIdBarcode());
			if (count == 0) {
				MedBarcodeSCOM registroReplicado = scomDao.save(medBarcodeSCOM);
				mensajeLog = "Se actualizó el barcode en el SCOM.";
				valorNuevo = registroReplicado.toString();
				LogWriter.escribirLog(mensajeLog);
				LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
			}
			LogWriter.escribirLog("Ya existe: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó el barcode en el SCOM: {}.", medBarcodeSCOM);
		}
	}
}
