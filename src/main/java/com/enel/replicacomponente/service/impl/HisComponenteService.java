package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedHisComponenteSC4J;
import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;
import com.enel.replicacomponente.entity.scom.MedHisComponenteSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedHisComponenteSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedHisComponenteSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("hisComponenteService")
public class HisComponenteService {

	@Autowired
	private MedHisComponenteSCOMRepository scomDao;
	@Autowired
	private MedHisComponenteSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(String fechaDesde, String fechaHasta, List<MedComponenteSCOM> medComponenteSCOMList) throws IOException {
		List<MedHisComponenteSCOM> registrosPorReplicar = new ArrayList<>();
		for (MedComponenteSCOM medComponenteSCOM : medComponenteSCOMList) {
			registrosPorReplicar.addAll(scomDao.obtenerRegistros(medComponenteSCOM.getId()));
		}
		if (!registrosPorReplicar.isEmpty()) {
			List<MedHisComponenteSC4J> registrosReplicados = replicarRegistros(registrosPorReplicar);
			replicarEnSC4J(registrosReplicados);
		}
	}
	
	private List<MedHisComponenteSC4J> replicarRegistros(List<MedHisComponenteSCOM> registrosPorReplicar) {
		List<MedHisComponenteSC4J> nuevoMedHisComponenteSC4JList = new ArrayList<>();
		MedHisComponenteSC4J nuevoMedHisComponenteSC4J;
		for (MedHisComponenteSCOM registroPorReplicar : registrosPorReplicar) {
			nuevoMedHisComponenteSC4J = new MedHisComponenteSC4J();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedHisComponenteSC4J);
			nuevoMedHisComponenteSC4JList.add(nuevoMedHisComponenteSC4J);
		}
		return nuevoMedHisComponenteSC4JList;
	}

	private void replicarEnSC4J(List<MedHisComponenteSC4J> medHisComponenteSC4JList) throws IOException {
		String valorNuevo = "";
		MedHisComponenteSC4J registroReplicado;
		for (MedHisComponenteSC4J medHisComponenteSC4J : medHisComponenteSC4JList) {
			try {
				//Integer count = sc4jDao.countByIdHisComponente(medHisComponenteSC4J.getIdHisComponente());
				//if (count == 0) {
					registroReplicado = sc4jDao.save(medHisComponenteSC4J);
					mensajeLog = "Se actualizó el historial del componente en el SC4J.";
					valorNuevo = registroReplicado.toString();
					LogWriter.escribirLog(mensajeLog);
					LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				//}
				//LogWriter.escribirLog("Ya existe: ".concat(medHisComponenteSC4J.toString()));
			} catch (Exception e) {
				log.error("No se actualizó el historial del componente en el SC4J: {}.", medHisComponenteSC4J);
			}
		}
	}

	public List<Long> obtenerIdUltimosRegistros(String fechaDesde) {
		return scomDao.obtenerIdUltimosRegistros(fechaDesde);
	}

	/*
	public Optional<MedHisComponenteSCOM> obtenerHistorial(Long idHisComponente) {
		return scomDao.findById(idHisComponente);
	}
	*/
}
