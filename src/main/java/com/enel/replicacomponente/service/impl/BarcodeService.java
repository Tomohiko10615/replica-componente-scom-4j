package com.enel.replicacomponente.service.impl;

import java.io.IOException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedBarcodeSC4J;
import com.enel.replicacomponente.entity.scom.MedBarcodeSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedBarcodeSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedBarcodeSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("barcodeService")
public class BarcodeService {

	@Autowired
	private MedBarcodeSCOMRepository scomDao;
	@Autowired
	private MedBarcodeSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		MedBarcodeSCOM registroPorReplicar = scomDao.findByIdComponente(idComponente);
		if (registroPorReplicar != null) {
			//verificarExistenciaEnSC4J(idComponente);
			MedBarcodeSC4J registroReplicado = replicarRegistro(registroPorReplicar);
			replicarEnSC4J(registroReplicado);
		}
	}
	
	private void verificarExistenciaEnSC4J(Long idComponente) throws IOException {
		String valorAnterior = "";
		MedBarcodeSC4J medBarcodeSC4J = sc4jDao.findByIdComponente(idComponente).orElse(null);
		if (medBarcodeSC4J == null) {
			mensajeLog = 
					"El barcode no existe en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "El barcode existe en el SC4J. Se actualizará el registro.";
			valorAnterior = medBarcodeSC4J.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}
	
	private MedBarcodeSC4J replicarRegistro(MedBarcodeSCOM medBarcodeSCOM) {
		MedBarcodeSC4J nuevoMedBarcodeSC4J = new MedBarcodeSC4J();
		BeanUtils.copyProperties(medBarcodeSCOM, nuevoMedBarcodeSC4J);
		return nuevoMedBarcodeSC4J;
	}

	private void replicarEnSC4J(MedBarcodeSC4J medBarcodeSC4J) throws IOException {
		String valorNuevo = "";
		try {
			Integer count = sc4jDao.countByIdBarcode(medBarcodeSC4J.getIdBarcode());
			if (count == 0) {
				MedBarcodeSC4J registroReplicado = sc4jDao.save(medBarcodeSC4J);
				mensajeLog = "Se actualizó el barcode en el SC4J.";
				valorNuevo = registroReplicado.toString();
				LogWriter.escribirLog(mensajeLog);
				LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
			}
			LogWriter.escribirLog("Ya existe: ".concat(medBarcodeSC4J.toString()));
		} catch (Exception e) {
			log.error("No se actualizó el barcode en el SC4J: {}.", medBarcodeSC4J);
		}	
	}
}
