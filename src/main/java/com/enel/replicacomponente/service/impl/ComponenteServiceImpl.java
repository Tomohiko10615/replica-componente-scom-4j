package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enel.replicacomponente.dto.MedidorDTO;
import com.enel.replicacomponente.entity.sc4j.MedComponenteSC4J;
import com.enel.replicacomponente.entity.sc4j.MedHisComponenteSC4J;
import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedComponenteSC4JRepository;
import com.enel.replicacomponente.repository.scom.ComParametrosRepository;
import com.enel.replicacomponente.repository.scom.MedComponenteSCOMRepository;
import com.enel.replicacomponente.service.ComponenteService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ComponenteServiceImpl implements ComponenteService {
	
	@Autowired
	private MedComponenteSCOMRepository scomDao;
	@Autowired
	private MedComponenteSC4JRepository sc4jDao;
	
	@Autowired
	private ComParametrosRepository comParametrosRepository;
	
	private String mensajeLog = "";

	@Override
	public List<MedComponenteSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta) throws IOException {
		Long idHisComponenteInicial = comParametrosRepository.obtenerIdHisComponente("ID_HIS_INI");
		Long idHisComponenteFinal = comParametrosRepository.obtenerIdHisComponente("ID_HIS_FIN");
		
		//log.info("idHisComponenteInicial: {}", idHisComponenteInicial);
		log.info("idHisComponenteFinal: {}", idHisComponenteFinal);
		
		List<MedComponenteSCOM> medComponenteList = scomDao.obtenerUltimosRegistros(fechaDesde, fechaHasta, idHisComponenteFinal);
		if (medComponenteList.isEmpty()) {
			mensajeLog = "No se encontraron registros en med_componente para procesar.";
		} else {
			mensajeLog = "Se encontraron "
					.concat(String.valueOf(medComponenteList.size()))
					.concat(" registro(s) en med_componente para procesar.");
		}
		//LogWriter.escribirLog(mensajeLog);
		return medComponenteList;
	}

	@Override
	public void verificarExistenciaEnSC4J(Long id) throws IOException {
		String valorAnterior = "";
		MedComponenteSC4J medComponenteSC4J = sc4jDao.findById(id).orElse(null);
		if (medComponenteSC4J == null) {
			mensajeLog = 
					"El componente no existe en el SC4J. Se insertará como nuevo registro.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "El componente existe en el SC4J. Se actualizará el registro.";
			valorAnterior = medComponenteSC4J.toString();
		}
		//LogWriter.escribirLog(mensajeLog);
		//LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}

	@Override
	@Transactional
	public void replicarEnSC4J(MedComponenteSC4J medComponenteSC4J) throws IOException {
		String valorNuevo = "";
		try {
			MedComponenteSC4J registroReplicado = sc4jDao.save(medComponenteSC4J);
			mensajeLog = "Se actualizó el componente en el SC4J.";
			valorNuevo = registroReplicado.toString();
			//LogWriter.escribirLog(mensajeLog);
			//LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó el componente en el SC4J.", medComponenteSC4J);
		}
	}
	
	@Override
	public void replicarEnSCOM(MedComponenteSCOM medComponenteSCOM) throws IOException {
		String valorNuevo = "";
		try {
			MedComponenteSCOM registroReplicado = scomDao.save(medComponenteSCOM);
			mensajeLog = "Se actualizó el componente en el SCOM.";
			valorNuevo = registroReplicado.toString();
			//LogWriter.escribirLog(mensajeLog);
			//LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
		} catch (Exception e) {
			log.error("No se actualizó el componente en el SCOM: {}.", medComponenteSCOM);
		}
	}

	@Override
	public List<MedComponenteSC4J> obtenerUltimosRegistros4J(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal) throws IOException {
		return sc4jDao.obtenerUltimosRegistros4J(idMaxHisComponenteInicial, idMaxHisComponenteFinal);
	}
	
	@Override
	public List<MedComponenteSC4J> obtenerUltimosRegistros4J(int limit) {
		List<Long> idComponenteList = scomDao.obtenerIdsComponente4J(limit);
		return sc4jDao.findAllById(idComponenteList);
	}

	@Override
	public List<MedComponenteSCOM> obtenerRegistrosTemporales() {
		return scomDao.obtenerRegistrosTemporales();
	}

	@Override
	public Long obtenerIdMaxHisComponenteFinal(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal) {
		return sc4jDao.obtenerIdMaxHisComponenteFinal(idMaxHisComponenteInicial, idMaxHisComponenteFinal);
	}

}
