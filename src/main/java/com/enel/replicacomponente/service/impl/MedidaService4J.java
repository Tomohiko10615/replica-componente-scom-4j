package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.MedMedidaMedidorSC4J;
import com.enel.replicacomponente.entity.scom.MedMedidaMedidorSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.MedMedidaMedidorSC4JRepository;
import com.enel.replicacomponente.repository.scom.MedMedidaMedidorSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("medidaService4J")
public class MedidaService4J {

	@Autowired
	private MedMedidaMedidorSCOMRepository scomDao;
	@Autowired
	private MedMedidaMedidorSC4JRepository sc4jDao;
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		List<MedMedidaMedidorSC4J> registrosPorReplicar = sc4jDao.findByIdComponente(idComponente);
		scomDao.deleteByIdComponente(idComponente);
		List<MedMedidaMedidorSCOM> registrosReplicados = replicarRegistros(registrosPorReplicar);
		replicarEnSCOM(registrosReplicados);
	}
	
	private List<MedMedidaMedidorSCOM> replicarRegistros(List<MedMedidaMedidorSC4J> registrosPorReplicar) {
		List<MedMedidaMedidorSCOM> nuevoMedMedidaMedidorSCOMList = new ArrayList<>();
		MedMedidaMedidorSCOM nuevoMedMedidaMedidorSCOM;
		for (MedMedidaMedidorSC4J registroPorReplicar : registrosPorReplicar) {
			nuevoMedMedidaMedidorSCOM = new MedMedidaMedidorSCOM();
			BeanUtils.copyProperties(registroPorReplicar, nuevoMedMedidaMedidorSCOM);
			nuevoMedMedidaMedidorSCOM.setId(registroPorReplicar.getIdMedidaMedidor());
			nuevoMedMedidaMedidorSCOMList.add(nuevoMedMedidaMedidorSCOM);
		}
		return nuevoMedMedidaMedidorSCOMList;
	}

	private void replicarEnSCOM(List<MedMedidaMedidorSCOM> medMedidaMedidorSCOMList) throws IOException {
		String mensajeLog = "";
		String valorNuevo = "";
		MedMedidaMedidorSCOM registroReplicado;
		for (MedMedidaMedidorSCOM medMedidaMedidorSCOM : medMedidaMedidorSCOMList) {
			try {
				Integer count = scomDao.countById(medMedidaMedidorSCOM.getId());
				if (count == 0) {
					registroReplicado = scomDao.save(medMedidaMedidorSCOM);
					mensajeLog = "Se actualizó la medida del medidor en el SCOM.";
					valorNuevo = registroReplicado.toString();
					//LogWriter.escribirLog(mensajeLog);
					//LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				}
				//LogWriter.escribirLog("Ya existe: ".concat(medMedidaMedidorSCOM.toString()));
			} catch (Exception e) {
				log.error("No ee actualizó la medida del medidor en el SCOM: {}.", e.getMessage());
			}
		}
	}
}
