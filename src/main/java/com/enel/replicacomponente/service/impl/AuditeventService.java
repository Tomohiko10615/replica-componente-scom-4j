package com.enel.replicacomponente.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.enel.replicacomponente.entity.sc4j.FwkAuditeventSC4J;
import com.enel.replicacomponente.entity.scom.FwkAuditeventSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.repository.sc4j.FwkAuditeventSC4JRepository;
import com.enel.replicacomponente.repository.scom.FwkAuditeventSCOMRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("auditeventService")
public class AuditeventService {

	@Autowired
	private FwkAuditeventSCOMRepository scomDao;
	@Autowired
	private FwkAuditeventSC4JRepository sc4jDao;
	
	private String mensajeLog = "";
	
	public void replicarRegistroAsociado(Long idComponente) throws IOException {
		List<FwkAuditeventSCOM> registrosPorReplicar = scomDao.findByIdFk(idComponente);
		if (!registrosPorReplicar.isEmpty()) {
			//verificarExistenciaEnSC4J(idComponente);
			List<FwkAuditeventSC4J> registrosReplicado = replicarRegistros(registrosPorReplicar);
			replicarEnSC4J(registrosReplicado);
		}
	}
	
	private void verificarExistenciaEnSC4J(Long idComponente) throws IOException {
		String valorAnterior = "";
		List<FwkAuditeventSC4J> fwkAuditeventSC4JList = sc4jDao.findByIdFk(idComponente);
		if (fwkAuditeventSC4JList.isEmpty()) {
			mensajeLog = 
					"No existen auditorías en el SC4J. Se insertarán como nuevos registros.";
			valorAnterior = "No hay registro previo.";
		} else {
			mensajeLog = "Las auditorías existen en el SC4J. Se actualizarán los registros.";
			valorAnterior = fwkAuditeventSC4JList.toString();
		}
		LogWriter.escribirLog(mensajeLog);
		LogWriter.escribirLog("Valor anterior: ".concat(valorAnterior));
	}
	
	private List<FwkAuditeventSC4J> replicarRegistros(List<FwkAuditeventSCOM> registrosPorReplicar) {
		List<FwkAuditeventSC4J> nuevoFwkAuditeventSC4JList = new ArrayList<>();
		FwkAuditeventSC4J nuevoFwkAuditeventSC4J;
		for (FwkAuditeventSCOM registroPorReplicar : registrosPorReplicar) {
			nuevoFwkAuditeventSC4J = new FwkAuditeventSC4J();
			BeanUtils.copyProperties(registroPorReplicar, nuevoFwkAuditeventSC4J);
			nuevoFwkAuditeventSC4J.setIdAuditevent(registroPorReplicar.getId());
			nuevoFwkAuditeventSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
			nuevoFwkAuditeventSC4JList.add(nuevoFwkAuditeventSC4J);
		}
		return nuevoFwkAuditeventSC4JList;
	}

	private void replicarEnSC4J(List<FwkAuditeventSC4J> fwkAuditeventSC4JList) {
		String valorNuevo = "";
		FwkAuditeventSC4J registroReplicado;
		for (FwkAuditeventSC4J fwkAuditeventSC4J : fwkAuditeventSC4JList) {
			try {
				Integer count = sc4jDao.countByIdAuditevent(fwkAuditeventSC4J.getIdAuditevent());
				if (count == 0) {
					registroReplicado = sc4jDao.save(fwkAuditeventSC4J);
					mensajeLog = "Se actualizó la auditoría en el SC4J.";
					valorNuevo = registroReplicado.toString();
					LogWriter.escribirLog(mensajeLog);
					LogWriter.escribirLog("Valor actualizado: ".concat(valorNuevo));
				}
				LogWriter.escribirLog("Ya existe: ".concat(valorNuevo));
			} catch (Exception e) {
				log.error("No se actualizó la auditoría en el SC4J.", fwkAuditeventSC4J);
			}
		}
	}
}
