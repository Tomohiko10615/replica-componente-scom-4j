package com.enel.replicacomponente.service;

import java.io.IOException;
import java.util.List;

import com.enel.replicacomponente.entity.sc4j.MedModeloSC4J;
import com.enel.replicacomponente.entity.scom.MedModeloSCOM;

public interface ModeloService {

	List<MedModeloSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta) throws IOException;

	void verificarExistenciaEnSC4J(Long id) throws IOException;

	void replicarEnSC4J(MedModeloSC4J registroReplicado) throws IOException;

}
