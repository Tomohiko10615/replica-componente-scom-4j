package com.enel.replicacomponente.service;

import java.io.IOException;
import java.util.List;

import com.enel.replicacomponente.entity.sc4j.MedComponenteSC4J;
import com.enel.replicacomponente.entity.sc4j.MedHisComponenteSC4J;
import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;

public interface ComponenteService {

	List<MedComponenteSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta) throws IOException;

	void verificarExistenciaEnSC4J(Long id) throws IOException;

	void replicarEnSC4J(MedComponenteSC4J registroReplicado) throws IOException;

	void replicarEnSCOM(MedComponenteSCOM registroReplicado) throws IOException;
	
	List<MedComponenteSC4J> obtenerUltimosRegistros4J(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal)
			throws IOException;

	List<MedComponenteSCOM> obtenerRegistrosTemporales();

	Long obtenerIdMaxHisComponenteFinal(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal);

	List<MedComponenteSC4J> obtenerUltimosRegistros4J(int limit);

}
