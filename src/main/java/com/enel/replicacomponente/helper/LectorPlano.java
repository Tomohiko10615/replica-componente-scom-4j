package com.enel.replicacomponente.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LectorPlano {
	
	private LectorPlano() {
		super();
	}
	
	public static List<Long> obtenerIdComponenteList(String archivoPlano) {
		List<Long> idComponenteList = new ArrayList<>();
		FileReader reader = null;
		try {
			reader = new FileReader(archivoPlano);
		} catch (IOException e) {
			log.error("Error: No se encontró el archivo");
			e.printStackTrace();
			System.exit(1);
		}
		try (BufferedReader archivoPlanoReader = new BufferedReader(reader)) {
			String linea = archivoPlanoReader.readLine();
			while(linea != null) {
				idComponenteList.add(Long.parseLong(linea));
				linea = archivoPlanoReader.readLine();
			}
		} catch (IOException e) {
			log.error("Error: No se pudo leer el archivo");
			e.printStackTrace();
		}
		return idComponenteList;
	}
}
