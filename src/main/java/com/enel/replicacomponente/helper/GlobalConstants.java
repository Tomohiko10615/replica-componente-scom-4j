package com.enel.replicacomponente.helper;

public class GlobalConstants {

	private GlobalConstants() {
		super();
	}
	
	public static final int SUCCESSFUL_EXECUTION = 0;
	public static final int FAILED_EXECUTION = 1;
	public static final String TITLE = 
			  "###########################################\n"
			+ "##### REPLICA COMPONENTES SCOM - SC4J #####\n"
			+ "###########################################\n";
	public static final Long ID_ENEL = 3L;
	public static final String VALID_DATE_FORMAT = "dd/MM/yyyy";
}
