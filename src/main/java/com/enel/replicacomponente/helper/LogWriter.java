package com.enel.replicacomponente.helper;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.ResourceBundle;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogWriter {

	private LogWriter() {
		super();
	}

	private static final String LOG_FILE_NAME = "/ReplicaComponente_";
	private static final String DATE_FORMAT = "yyyy_MM_dd_HH_mm_ss";
	private static final Path ROOT_PATH = FileSystems.getDefault().getRootDirectories().iterator().next();
	private static String logFile;
	
	public static void iniciarLog() {
		try {
			Path logDirPath = Paths.get(ROOT_PATH + "/eorder2/logs/replica-componente");
			String now = getDateTimeSufix();
			createLogFile(logDirPath, now);
		} catch (IOException e) {
			//log.warn("Cannot create output path or file");
			//e.printStackTrace();
		}
	}
	
	private static String getDateTimeSufix() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);  
		LocalDateTime currentLocalDateTime = LocalDateTime.now();
		return dtf.format(currentLocalDateTime);
	}

	private static void createLogFile(Path logDirPath, String now) throws IOException {
		logFile = logDirPath + LOG_FILE_NAME + now + ".log";
		Path logFilePath = Paths.get(logFile);
		//Files.createDirectory(logDirPath);
	    Files.createFile(logFilePath);
	}
	
	public static void escribirLog(String textToWrite) {
		log.info(textToWrite);
		try(PrintWriter writer = 
				new PrintWriter(
						new FileWriter(logFile, true))) {
			writer.write(textToWrite);
		} catch (IOException e) {
			//e.printStackTrace();
			//log.warn("Cannot write to file");
		}
	}
}
