package com.enel.replicacomponente.entity.scom;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "dyo_object")
public class DyoObjectSCOM {

	@Id
	private Long id;
}
