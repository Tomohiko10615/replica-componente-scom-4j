package com.enel.replicacomponente.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "com_parametros")
public class ComParametros {

	@Id
	private Long id;
	
	private String sistema;
	private String entidad;
	
	@Column(name = "valor_alf")
	private String valorAlf;
}
