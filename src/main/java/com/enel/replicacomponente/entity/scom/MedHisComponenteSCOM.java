package com.enel.replicacomponente.entity.scom;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "med_his_componente")
public class MedHisComponenteSCOM {

	@Id
	@Column(name = "ID_HIS_COMPONENTE")
	private Long idHisComponente;
	
	@Column(name = "ID_COMPONENTE")
	private Long idComponente;
	
	@Column(name = "ID_EST_COMPONENTE")
	private Long idEstComponente;
	
	@Column(name = "FEC_DESDE")
	private Date fecDesde;
	
	@Column(name = "FEC_HASTA")
	private Date fecHasta;
	
	@Column(name = "ID_UBICACION")
	private Long idUbicacion;
	
	@Column(name = "TYPE_UBICACION")
	private String typeUbicacion;
	
	@Column(name = "ID_ORDEN")
	private Long idOrden;
								
}
