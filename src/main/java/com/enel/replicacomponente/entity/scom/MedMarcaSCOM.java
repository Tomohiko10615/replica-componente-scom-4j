package com.enel.replicacomponente.entity.scom;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "med_marca")
public class MedMarcaSCOM {

	@Id
	private Long id;
	
	@Column(name = "id_tip_componente")
	private Long idTipComponente;
	
	@Column(name = "cod_marca")
	private String codMarca;
	
	@Column(name = "des_marca")
	private String desMarca;
	
	private String activo;
	
	@Column(name = "id_usuario_registro")
	private Long idUsuarioRegistro;
	
	@Column(name = "fec_registro")
	private Date fecRegistro;
	
	@Column(name = "id_usuario_modif")
	private Long idUsuarioModif;
	
	@Column(name = "fec_modif")
	private Date fecModif;

}
