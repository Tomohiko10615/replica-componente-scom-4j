package com.enel.replicacomponente.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.enel.replicacomponente.entity.sc4j.AccountId;

import lombok.Data;

@Entity
@IdClass(AccountId.class)
@Data
@Table(name = "med_fac_med_mod")
public class MedFacMedModSCOM {

	@Id
	@Column(name = "id_medida_modelo")
	private Long idMedidaModelo;
	
	@Id
	@Column(name = "id_factor")
	private Long idFactor;
	
}
