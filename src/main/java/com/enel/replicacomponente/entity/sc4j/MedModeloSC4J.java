package com.enel.replicacomponente.entity.sc4j;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "med_modelo")
public class MedModeloSC4J {

	@Id
	@Column(name = "id_modelo")
	private Long idModelo;
	
	@Column(name = "id_marca")
	private Long idMarca;
	
	@Column(name = "cod_modelo")
	private String codModelo;
	
	@Column(name = "des_modelo")
	private String desModelo;
	
	@Column(name = "id_tip_modelo")
	private Long idTipModelo;
	
	@Column(name = "fec_creacion")
	private Date fecCreacion;
	
	@Column(name = "cant_anos_vida")
	private Integer canAnosVida;
	
	@Column(name = "cant_anos_almacen")
	private Integer cantAnosAlmacen;
	
	@Column(name = "nro_sellos_cert")
	private Integer nroSellosCert;
	
	@Column(name = "nro_sellos_inst")
	private Integer nroSellosInst;
	
	@Column(name = "id_fase")
	private Long idFase;
	
	@Column(name = "id_tip_medicion")
	private Long idTipMedicion;
	
	@Column(name = "id_voltaje")
	private Long idVoltaje;
	
	@Column(name = "id_tension")
	private Long idTension;
	
	@Column(name = "id_tecnologia")
	private Long idTecnologia;
	
	@Column(name = "id_tip_registrador")
	private Long idTipRegistrador;
	
	@Column(name = "nro_registrador")
	private Long nroRegistrador;
	
	@Column(name = "es_reacondicionado")
	private String esReacondicionado;
	
	@Column(name = "es_reseteado")
	private String esReseteado;
	
	@Column(name = "es_patron")
	private String esPatron;
	
	@Column(name = "es_totalizador")
	private String esTotalizador;
	
	private Double exactitud;
	
	private Double constante;
	
	@Column(name = "id_amperaje")
	private Long idAmperaje;
	
	@Column(name = "id_unidad_medida_cte")
	private Long idUnidadMedidaCte;
	
	@Column(name = "cant_hilos")
	private Long cantHilos;
	
	private String activo;
	
	@Column(name = "id_dynamicobject")
	private Long idDynamicobject;
	
	private String discriminador;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;
	
}
