package com.enel.replicacomponente.entity.sc4j;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountId implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long idMedidaModelo;
    private Long idFactor;

    // default constructor

    /*
    public AccountId(Long idMedidaModelo, Long idFactor) {
        this.idMedidaModelo = idMedidaModelo;
        this.idFactor = idFactor;
    }
    */

    // equals() and hashCode()
}
