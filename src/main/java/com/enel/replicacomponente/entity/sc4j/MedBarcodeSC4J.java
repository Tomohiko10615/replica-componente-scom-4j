package com.enel.replicacomponente.entity.sc4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "med_barcode")
public class MedBarcodeSC4J {

	@Id
	@Column(name = "id_barcode")
	private Long idBarcode;
	
	@Column(name = "id_componente")
	private Long idComponente;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;
	
	@Column(name = "cod_primario_pallet")
	private String codPrimarioPallet;
	
	@Column(name = "cod_secundario_pallet")
	private String codSecundarioPallet;
	
	@Column(name = "cod_primario_caja")
	private String codPrimarioCaja;
	
	@Column(name = "cod_secundario_caja")
	private String codSecundarioCaja;
	
	@Column(name = "cod_medidor")
	private String codMedidor;
						
}
