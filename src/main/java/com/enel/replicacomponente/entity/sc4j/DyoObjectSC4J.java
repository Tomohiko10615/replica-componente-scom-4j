package com.enel.replicacomponente.entity.sc4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "dyo_object")
public class DyoObjectSC4J {

	@Id
	@Column(name = "id_object")
	private Long idObject;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;
}
