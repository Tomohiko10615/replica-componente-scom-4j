package com.enel.replicacomponente.entity.sc4j;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "med_calibracion")
public class MedCalibracionSC4J {

	@Id
	@Column(name = "id_calibracion")
	private Long idCalibracion;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;
	
	@Column(name = "id_componente")
	private Long idComponente;
	
	@Column(name = "fec_calibracion")
	private Date fecCalibracion;
	
	@Column(name = "calibracion_01")
	private Double calibracion01;
	
	@Column(name = "calibracion_02")
	private Double calibracion02;
	
	@Column(name = "calibracion_03")
	private Double calibracion03;
	
	@Column(name = "calibracion_04")
	private Double calibracion04;
	
	@Column(name = "calibracion_05")
	private Double calibracion05;
	
	@Column(name = "calibracion_06")
	private Double calibracion06;
	
	@Column(name = "id_ejecutor")
	private Long idEjecutor;
	
	@Column(name = "nro_protocolo")
	private Long nroProtocolo;
	
	@Column(name = "id_laboratorio")
	private Long idLaboratorio;
	
	@Column(name = "nro_resultado_calibracion")
	private Long nroResultadoCalibracion;
	
	@Column(name = "nro_etiqueta_calibracion")
	private Long nroEtiquetaCalibracion;
	
	@Column(name = "resultado_calibracion")
	private String resultadoCalibracion;
	
	@Column(name = "id_observacion")
	private Long idObservacion;
	
	@Column(name = "id_usuario_modificacion")
	private Long idUsuarioModificacion;
	
	@Column(name = "type_componente")
	private String typeComponente;
		
}
