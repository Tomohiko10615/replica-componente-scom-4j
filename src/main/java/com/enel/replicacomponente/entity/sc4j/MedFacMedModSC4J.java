package com.enel.replicacomponente.entity.sc4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Entity
@IdClass(AccountId.class)
@Data
@Table(name = "med_fac_med_mod")
public class MedFacMedModSC4J {

@Id
@Column(name = "id_medida_modelo")
	private Long idMedidaModelo;
	
@Id
@Column(name = "id_factor")
	private Long idFactor;
	
}
