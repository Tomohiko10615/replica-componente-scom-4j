package com.enel.replicacomponente.entity.sc4j;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import lombok.ToString;
@Data
@Entity
@ToString
@Table(name = "FWK_AUDITEVENT")
public class FwkAuditeventSC4J {
    @Id
    @Column(name = "ID_AUDITEVENT")
    private Long idAuditevent;
	@Column(name = "usecase")
    private String usecase;
	@Column(name = "objectref")
    private String objectref;
	@Column(name = "id_fk")
    private Long idFk;
	@Column(name = "fecha_ejecucion")
    private Date fechaejecucion;
	@Column(name = "specific_auditevent")
    private String specificAuditevent;
	@Column(name = "id_user")
    private Long idUser;
    @Column(name = "ID_EMPRESA")
    private Long idEmpresa;
}
