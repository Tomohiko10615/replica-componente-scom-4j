package com.enel.replicacomponente.entity.sc4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "med_medida_modelo")
public class MedMedidaModeloSC4J {

	@Id
	@Column(name = "ID_MEDIDA_MODELO")
	private Long idMedidaModelo;

	@Column(name = "id_modelo")
	private Long idModelo;
	
	@Column(name = "id_medida")
	private Long idMedida;
	
	@Column(name = "id_ent_dec")
	private Long idEntDec;

	@Column(name = "id_tip_calculo")
	private Long idTipCalculo;
	
	@Column(name = "constante")
	private Double constante;
	
	@Column(name = "id_unidad_medida_cte")
	private Long idUnidadMedidaCte;

	@Column(name = "id_clase_precision")
	private Long idClasePrecision;
		
	@Column(name = "ID_EMPRESA")
	private Long idEmpresa;
											
}
