package com.enel.replicacomponente.entity.sc4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "med_medida_medidor")
public class MedMedidaMedidorSC4J {

	@Id
	@Column(name = "ID_MEDIDA_MEDIDOR")
	private Long idMedidaMedidor;
	
	@Column(name = "ID_EMPRESA")
	private Long idEmpresa;
	
	@Column(name = "ID_COMPONENTE")
	private Long idComponente;
	
	@Column(name = "ID_MEDIDA")
	private Long idMedida;
	
	@Column(name = "ID_ENT_DEC")
	private Long idEntDec;
	
	@Column(name = "CANT_ENTEROS")
	private Integer cantEnteros;
	
	@Column(name = "CANT_DECIMALES")
	private Integer cantDecimales;
	
	@Column(name = "ID_FACTOR")
	private Long idFactor;
	
	@Column(name = "VAL_FACTOR")
	private Double valFactor;
	
	@Column(name = "ID_TIP_CALCULO")
	private Long idTipCalculo;
										
}
