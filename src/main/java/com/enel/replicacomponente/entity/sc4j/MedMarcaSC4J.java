package com.enel.replicacomponente.entity.sc4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
@Table(name = "med_marca")
public class MedMarcaSC4J {

	@Id
	@Column(name = "id_marca")
	private Long idMarca;
	
	@Column(name = "id_tip_componente")
	private Long idTipComponente;
	
	@Column(name = "cod_marca")
	private String codMarca;
	
	@Column(name = "des_marca")
	private String desMarca;
	
	private String activo;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;

}
