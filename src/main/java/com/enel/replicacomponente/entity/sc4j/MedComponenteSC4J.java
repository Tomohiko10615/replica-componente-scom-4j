package com.enel.replicacomponente.entity.sc4j;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "med_componente")
public class MedComponenteSC4J {

	@Id
	@Column(name = "id_componente")
	private Long idComponente;
	
	@Column(name = "id_modelo")
	private Long idModelo;
	
	@Column(name = "id_est_componente")
	private Long idEstComponente;
	
	@Column(name = "fec_creacion")
	private Date fecCreacion;
	
	@Column(name = "id_propiedad")
	private Long idPropiedad;
	
	@Column(name = "ano_fabricacion")
	private Integer anoFabricacion;
	
	@Column(name = "nro_fabrica")
	private Long nroFabrica;
	
	@Column(name = "id_accion_realizada")
	private Long idAccionRealizada;
	
	@Column(name = "id_cond_creacion")
	private Long idCondCreacion;
	
	@Column(name = "es_control_interno")
	private String esControlInterno;
	
	@Column(name = "es_prototipo")
	private String esPrototipo;
	
	@Column(name = "id_tip_transfor")
	private Long idTipTransfor;
	
	@Column(name = "cod_tip_componente")
	private String codTipComponente;
	
	@Column(name = "id_equipo")
	private Long idEquipo;
	
	@Column(name = "id_resp_medicion")
	private Long idRespMedicion;
	
	@Column(name = "id_mod_medicion")
	private Long idModMedicion;
	
	@Column(name = "tel_medicion")
	private String telMedicion;
	
	@Column(name = "ubi_instalacion")
	private String ubiInstalacion;
	
	@Column(name = "id_ubicacion")
	private Long idUbicacion;
	
	@Column(name = "type_ubicacion")
	private String typeUbicacion;
	
	@Column(name = "id_dynamicobject")
	private Long idDynamicobject;
	
	@Column(name = "hora_reloj")
	private Date horaReloj;
	
	@Column(name = "hora_verif")
	private Date horaVerif;
	
	@Column(name = "id_relacion")
	private Long idRelacion;
	
	@Column(name = "id_nev")
	private Long idNev;
	
	@Column(name = "id_tip_medida")
	private Long idTipMedida;
	
	@Column(name = "nro_componente")
	private String nroComponente;
	
	@Column(name = "ubicacion_inst_medidor")
	private String ubicacionInstMedidor;
	
	@Column(name = "fecha_suministro")
	private Date fechaSuministro;
	
	@Column(name = "suministrador_componente")
	private String suministradorComponente;
	
	private String reacondicionado;
	
	@Column(name = "id_motivo_baja")
	private Long idMotivoBaja;
	
	@Column(name = "factor_interno")
	private Double factorInterno;
	
	@Column(name = "serial_number")
	private String serialNumber;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;
	
}
