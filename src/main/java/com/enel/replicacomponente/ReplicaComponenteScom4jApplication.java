package com.enel.replicacomponente;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.helper.LectorPlano;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.replicator.Componente4JReplicator;
import com.enel.replicacomponente.replicator.ComponenteReplicator;
import com.enel.replicacomponente.replicator.MarcaReplicator;
import com.enel.replicacomponente.replicator.MedidaModeloReplicator;
import com.enel.replicacomponente.replicator.ModeloReplicator;
import com.enel.replicacomponente.repository.scom.ComParametrosRepository;
import com.enel.replicacomponente.repository.scom.MedComponenteSCOMRepository;
import com.enel.replicacomponente.service.ComponenteService;
import com.enel.replicacomponente.service.impl.MedidaService4J;

import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.agent.builder.AgentBuilder.FallbackStrategy.Simple;

@SpringBootApplication
@Slf4j
public class ReplicaComponenteScom4jApplication implements CommandLineRunner {

	@Autowired
	private ComponenteReplicator componenteReplicator;
	@Autowired
	private MarcaReplicator marcaReplicator;
	@Autowired
	private MedidaModeloReplicator medidaModeloReplicator;
	@Autowired
	private ModeloReplicator modeloReplicator;
	
	@Autowired
	private ComponenteService componenteService;
	
	@Autowired
	private Componente4JReplicator componente4jReplicator;
	
	@Autowired
	private ComParametrosRepository comParametrosRepository;
	
	@Autowired
	private MedComponenteSCOMRepository medComponenteSCOMRepository;
	
	private static final String COD_MODIF = "IDMHISC";
	
	public static void main(String[] args) {
		SpringApplication.run(ReplicaComponenteScom4jApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// para probar desde el vs code - borrar
		// args = new String[3];
		// args[0] = "11/04/2023";
		// args[1] = "01/01/2023";
		// args[2] = "D:/cfdiaze/indra/enel/proyecto/impactoscomcnrreqmejorasnegociomarket50/replicamed021123.txt";
		// System.exit(1);
		
		/* LANZANDO EL PROCESO CON ARCHIVO PLANO
		 * Línea de ejecución:
		 * java -jar /binarios/ejecutables/SCOM/eorder/replica/replica-componente-scom-4j.jar 10/05/2023 01/01/2023 /tmp/indra/up/med09052023.txt >> "/eorder2/logs/replica-componente/replicacomponenteplano`date +%Y%m%d%H%M%S`.log" &
		 * args[0] es la fechaHasta, debe ser el día de mañana
		 * args[1] es la fechaDesde, se usa para limitar el número de registros de med_his_componente, sino hay ninguna indicación especial usar 01/01/2023
		 * args[2] es la ruta del archivo plano
		 */

		//medidaService4J.replicarRegistroAsociado(32167412021238L);
		
		LogWriter.iniciarLog();
		LogWriter.escribirLog(GlobalConstants.TITLE);
		
		if (args.length > 3) {
			System.exit(1);
		}
		
		String archivoPlano = "";
		String fechaDesde = "";
		String fechaHasta = "";
		
		boolean leerPlano = false;
		boolean replica4J = false;
		Long idMaxHisComponenteInicial;
		Long idMaxHisComponenteFinal;
		int limit = 0;
		
		// java -jar /binarios/ejecutables/SCOM/eorder/replica/replica-componente-scom-4j.jar >> "/eorder2/logs/replica-componente/replicacomponente4J`date +%Y%m%d%H%M%S`.log" &
		// Homologación 4J SCOM
		if (args.length == 2) {
			replica4J = true;
			limit = Integer.parseInt(args[0]);
		}
		
		// Flujo Normal
		if (args.length == 1) {
			fechaHasta = args[0];
		}
		
		// Plano SCOM 4J
		if (args.length == 3) {
			leerPlano = true;
			fechaHasta = args[0];
			fechaDesde = args[1];
			archivoPlano = args[2];
		}
		
		List<MedComponenteSCOM> medComponenteSCOMList;
		
		if (leerPlano) {
			List<Long> idComponenteList = LectorPlano.obtenerIdComponenteList(archivoPlano);
			medComponenteSCOMList = medComponenteSCOMRepository.findAllById(idComponenteList);
			componenteReplicator.replicar(fechaDesde, fechaHasta, medComponenteSCOMList);
		} else if (replica4J) {
			componente4jReplicator.replicar(limit);
		} else {
			fechaDesde = comParametrosRepository.obtenerUlltimaFechaModif();
			medComponenteSCOMList =
					componenteService.obtenerUltimosRegistros(fechaDesde, fechaHasta);
			log.info(medComponenteSCOMList.toString());
			log.info("{}", medComponenteSCOMList.size());
			Date maxFecha = new Date(Long.MIN_VALUE);
			boolean foundMaxDate = false;
			for (MedComponenteSCOM medComponenteSCOM : medComponenteSCOMList) {
			    if (medComponenteSCOM.getFecModif().after(maxFecha)) {
			        maxFecha = medComponenteSCOM.getFecModif();
			        foundMaxDate = true;
			    }
			}
			idMaxHisComponenteInicial = comParametrosRepository.obtenerIdMaxHisComponenteInicial(COD_MODIF);
			//idMaxHisComponenteFinal = 3500080320133652L;
			idMaxHisComponenteFinal = comParametrosRepository.obtenerIdHisComponente("ID_HIS_FIN");
			log.info("idHisComponenteInicialVariable: {}", idMaxHisComponenteInicial);
			log.info("idHisComponenteFinal: {}", idMaxHisComponenteFinal);
			//idMaxHisComponenteFinal = 1750040162359119L;
			//idMaxHisComponenteFinal = 1750040160068010L;
			Long idMaxHisComponente = componente4jReplicator.replicar(idMaxHisComponenteInicial, idMaxHisComponenteFinal);
			if (idMaxHisComponente != null) {
				comParametrosRepository.updateIdMaxHisComponenteInicial(COD_MODIF, idMaxHisComponente);
			}
			componenteReplicator.replicar(fechaDesde, fechaHasta, medComponenteSCOMList);
			marcaReplicator.replicar(fechaDesde, fechaHasta);
			modeloReplicator.replicar(fechaDesde, fechaHasta);
			if (foundMaxDate) {
			    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			    String maxFechaString = dateFormat.format(maxFecha);
			    comParametrosRepository.actualizarFechaModificacion(maxFechaString);
			    log.info("fecha actualizada: {}", maxFechaString);
			}
			medidaModeloReplicator.replicar();
		}
		log.info("El proceso se ejecutó con éxito.");
		
		System.exit(GlobalConstants.SUCCESSFUL_EXECUTION);
	}
}
