package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.entity.scom.MedMedidaModeloSCOM;

public interface MedMedidaModeloSCOMRepository extends JpaRepository<MedMedidaModeloSCOM, Long> {

	List<MedMedidaModeloSCOM> findByIdModelo(Long IdModelo);	
	
	/*
	@Query(value = "select * from schscom.med_medida_modelo where id in (select max(mmm.id) from schscom.med_medida_modelo mmm \r\n"
			+ "group by mmm.id_modelo, mmm.id_medida\r\n"
			+ "having min(mmm.id) <> max(mmm.id))", nativeQuery = true)
	List<MedMedidaModeloSCOM> obtenerUltimosRegistros(Long idMax);
	*/
	
	@Query(value = "select * from schscom.med_medida_modelo where id > ? and id_modelo is not null and id_tip_calculo is not null", nativeQuery = true)
	List<MedMedidaModeloSCOM> obtenerUltimosRegistros(Long idMax);

	@Query(value = "select count(1) from schscom.med_fac_med_mod ", nativeQuery = true)
	Long obtenerTotalFac();
	
	@Query(value = "select * from schscom.med_fac_med_mod ", nativeQuery = true)
	List<Object[]> obtenerRegistros();


}
