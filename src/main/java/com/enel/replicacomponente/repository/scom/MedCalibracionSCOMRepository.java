package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.scom.MedCalibracionSCOM;

public interface MedCalibracionSCOMRepository extends JpaRepository<MedCalibracionSCOM, Long> {

	List<MedCalibracionSCOM> findByIdComponente(Long idComponente);

	Integer countByIdCalibracion(Long idCalibracion);

}
