package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.dto.MedidaModeloFactorDTO;
import com.enel.replicacomponente.entity.scom.MedFacMedModSCOM;

public interface MedFacMedModSCOMRepository extends JpaRepository<MedFacMedModSCOM, Long>{
	List<MedFacMedModSCOM> findByIdMedidaModelo(Long idMedidaModelo);

	@Query(value = "select id_medida_modelo as idMedidaModelo, count(id_factor) as countIdFactor "
			+ "from schscom.med_fac_med_mod mfmm group by id_medida_modelo", nativeQuery = true)
	List<MedidaModeloFactorDTO> obtenerMedidaModeloFactor();

	List<MedFacMedModSCOM> findByIdMedidaModeloIn(List<Long> medModIdList);

}
