package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.dto.MedidorDTO;
import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;

public interface MedComponenteSCOMRepository extends JpaRepository<MedComponenteSCOM, Long>{

	/*
	@Query(value = 
    		"select * from schscom.med_componente where id in (select distinct(id_componente) from schscom.med_his_componente where 1=1\r\n"
    		+ "and (fec_hasta >= to_date(?1, 'dd/mm/yyyy') and fec_hasta < to_date(?2, 'dd/mm/yyyy'))\r\n"
    		+ "OR (fec_desde >= to_date(?1, 'dd/mm/yyyy') and fec_desde < to_date(?2, 'dd/mm/yyyy')))"
    	    , nativeQuery = true)
	List<MedComponenteSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta);
	*/
	
	@Query(value = "select mc.*\r\n"
			+ "\r\n"
			+ "from schscom.med_componente mc,\r\n"
			+ "\r\n"
			+ "schscom.med_his_componente mhc\r\n"
			+ "\r\n"
			+ "where mc.id = mhc.id_componente  \r\n"
			+ "\r\n"
			+ "and mc.fec_modif >=  to_timestamp(?1, 'DD/MM/YYYY HH24:MI:SS')\r\n"
			+ "\r\n"
			+ "and mc.fec_modif < to_timestamp(?2, 'DD/MM/YYYY HH24:MI:SS')\r\n"
			+ "\r\n"
			+ "and mhc.id_his_componente >= ?3", nativeQuery = true)
			//+ "where mhc.id_his_componente > 1750040160064589\r\n"
			//+ "and mhc.id_his_componente < 22279316336224152)", nativeQuery = true)
			//+ "where mhc.id_his_componente > 1750040160064589\r\n"
			//+ "and mhc.id_his_componente < 3500080320133652)", nativeQuery = true)
	List<MedComponenteSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta, Long idHisComponenteFinal);

	@Query(value = 
    		"select * from schscom.med_componente \r\n"
    		+ "where id in (select id_medidor from schscom.temp_med_magnitud_3 where id_ord_transfer in (\r\n"
    		+ "38397640,\r\n"
    		+ "40800051,\r\n"
    		+ "41494122,\r\n"
    		+ "82764909,\r\n"
    		+ "82843609,\r\n"
    		+ "37578484,\r\n"
    		+ "41152720,\r\n"
    		+ "41213177,\r\n"
    		+ "41227808,\r\n"
    		+ "41263418,\r\n"
    		+ "41263420,\r\n"
    		+ "41264174,\r\n"
    		+ "41264949,\r\n"
    		+ "41266653,\r\n"
    		+ "41267403,\r\n"
    		+ "41267877,\r\n"
    		+ "41336083,\r\n"
    		+ "41348264,\r\n"
    		+ "41351016,\r\n"
    		+ "41358260,\r\n"
    		+ "41364018,\r\n"
    		+ "41365876,\r\n"
    		+ "41379664,\r\n"
    		+ "41380450,\r\n"
    		+ "41390694,\r\n"
    		+ "41402332,\r\n"
    		+ "41412988,\r\n"
    		+ "41428651,\r\n"
    		+ "41429001,\r\n"
    		+ "41435666,\r\n"
    		+ "41439280,\r\n"
    		+ "41441569))"
    	    , nativeQuery = true)
	List<MedComponenteSCOM> obtenerRegistrosTemporales();

	@Query(value = 
    		"SELECT \r\n"
    		+ "         unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/numeroMedidor/text()',\r\n"
    		+ "                      d.REG_XML))\\:\\:text AS nroComponente,\r\n"
    		+ "         unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/modeloMedidor/text()',\r\n"
    		+ "                      d.REG_XML))\\:\\:text AS modelo,\r\n"
    		+ "         unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/marcaMedidor/text()',\r\n"
    		+ "                      d.REG_XML))\\:\\:text AS marca\r\n"
    		+ "  FROM schscom.eor_ord_transfer ot, schscom.EOR_ORD_TRANSFER_DET d\r\n"
    		+ "  WHERE ot.id_ord_transfer = d.id_ord_transfer\r\n"
    		+ "    and cod_estado_orden in (6,8)\r\n"
    		+ "    and cod_tipo_orden_legacy in ('OCNX') \r\n"
    		+ "    AND accion = 'RECEPCION'\r\n"
    		+ "    and ot.fec_operacion >= to_date(?1, 'dd/MM/yyyy')\r\n"
    		+ "    and ot.fec_operacion < to_date(?2, 'dd/MM/yyyy')"
    	    , nativeQuery = true)
	List<MedidorDTO> obtenerMedidores(String fechaDesde, String fechaHasta);

	@Query(value = 
    		"select tmcsj.id\r\n"
    		+ "from tmp_med_comp_sc4j tmcsj\r\n"
    		+ "where tmcsj.id not in(\r\n"
    		+ "select x.id_componente\r\n"
    		+ "from\r\n"
    		+ "(select mhc.id_componente, count(*)\r\n"
    		+ "        from schscom.med_his_componente mhc\r\n"
    		+ "        where mhc.id_his_componente > 1750040160064589\r\n"
    		+ "          and mhc.id_his_componente < 3500080320133652\r\n"
    		+ "group by mhc.id_componente) x\r\n"
    		+ ")\r\n"
    		+ "order by tmcsj.id\r\n"
    		+ "limit ?"
    	    , nativeQuery = true)
	List<Long> obtenerIdsComponente4J(int limit);

}
