package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.replicacomponente.entity.scom.MedMedidaMedidorSCOM;

public interface MedMedidaMedidorSCOMRepository extends JpaRepository<MedMedidaMedidorSCOM, Long> {

	List<MedMedidaMedidorSCOM> findByIdComponente(Long idComponente);

	Integer countById(Long id);

	int countByIdComponente(Long idComponente);

	@Transactional
	void deleteByIdComponente(Long idComponente);

}
