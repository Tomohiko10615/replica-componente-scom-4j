package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.entity.scom.MedMarcaSCOM;

public interface MedMarcaSCOMRepository extends JpaRepository<MedMarcaSCOM, Long>{

	@Query(value = 
    		"select * "
    	    + "from med_marca "
    	    + "where 1 = 1 "
    	    + "and ((fec_registro >= to_date(?1, 'dd/MM/yyyy') "
    	    + "and fec_registro < to_date(?2, 'dd/MM/yyyy') )"
    	    + "or (fec_modif >= to_date(?1, 'dd/MM/yyyy') "
    	    + "and fec_modif < to_date(?2, 'dd/MM/yyyy')))"
    	    , nativeQuery = true)
	List<MedMarcaSCOM> obtenerUltimosRegistros(String fechaDesde, String fechaHasta);

}
