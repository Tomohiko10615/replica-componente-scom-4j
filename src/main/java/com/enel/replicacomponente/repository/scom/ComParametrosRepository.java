package com.enel.replicacomponente.repository.scom;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.enel.replicacomponente.entity.scom.ComParametros;

public interface ComParametrosRepository extends JpaRepository<ComParametros, Long> {

	@Query(value = "select valor_alf from com_parametros where sistema = 'SCOM' and entidad = 'COMPONENTE' and codigo = 'MODIF'", nativeQuery = true)
	String obtenerUlltimaFechaModif();
	
	@Modifying
	@Transactional
	@Query(value = "update com_parametros set valor_alf = ? where sistema = 'SCOM' and entidad = 'COMPONENTE' and codigo = 'MODIF'", nativeQuery = true)
	void actualizarFechaModificacion(String ultimaFecModif);

	@Query(value = "select valor_num  \r\n"
			+ "\r\n"
			+ "from com_parametros  \r\n"
			+ "\r\n"
			+ "where sistema = 'SCOM'  \r\n"
			+ "\r\n"
			+ "and entidad = 'COMPONENTE'  \r\n"
			+ "\r\n"
			+ "and codigo = ?1", nativeQuery = true)
	Long obtenerIdMaxHisComponenteInicial(String codigo);

	@Modifying
	@Transactional
	@Query(value = "update com_parametros set valor_num = ?2 \r\n"
			+ "\r\n"
			+ "where sistema = 'SCOM'  \r\n"
			+ "\r\n"
			+ "and entidad = 'COMPONENTE'  \r\n"
			+ "\r\n"
			+ "and codigo = ?1 ", nativeQuery = true)
	void updateIdMaxHisComponenteInicial(String codigo, Long idMaxHisComponente);

	@Query(value = "select valor_num  \r\n"
			+ "\r\n"
			+ "from com_parametros  \r\n"
			+ "\r\n"
			+ "where sistema = 'SCOM'  \r\n"
			+ "\r\n"
			+ "and entidad = 'COMPONENTE'  \r\n"
			+ "\r\n"
			+ "and codigo = ?1", nativeQuery = true)
	Long obtenerIdHisComponente(String codigo);

}
