package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.scom.FwkAuditeventSCOM;

public interface FwkAuditeventSCOMRepository extends JpaRepository<FwkAuditeventSCOM, Long> {

	List<FwkAuditeventSCOM> findByIdFk(Long idComponente);

	Integer countById(Long id);

}
