package com.enel.replicacomponente.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.scom.DyoObjectSCOM;

public interface DyoObjectSCOMRepository extends JpaRepository<DyoObjectSCOM, Long> {

}
