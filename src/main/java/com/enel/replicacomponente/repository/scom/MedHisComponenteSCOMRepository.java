package com.enel.replicacomponente.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.entity.scom.MedHisComponenteSCOM;

public interface MedHisComponenteSCOMRepository extends JpaRepository<MedHisComponenteSCOM, Long> {

	List<MedHisComponenteSCOM> findByIdComponente(Long idComponente);

	@Query(value = 
    		"select distinct(id_componente)\r\n"
    		+ "from schscom.med_his_componente mhc where 1=1 \r\n"
    		+ "and (fec_hasta > to_date(?1, 'dd/MM/yyyy') and fec_hasta < current_timestamp) or \r\n"
    		+ "(fec_desde > to_date(?1, 'dd/MM/yyyy') and fec_desde < current_timestamp)"
    	    , nativeQuery = true)
	List<Long> obtenerIdUltimosRegistros(String fechaDesde);

	@Query(value = 
    		"select *\r\n"
    		+ "from schscom.med_his_componente mhc where 1=1 \r\n"
    		+ "and id_componente = ?1"
    	    , nativeQuery = true)
	List<MedHisComponenteSCOM> obtenerRegistros(Long idComponente);

}
