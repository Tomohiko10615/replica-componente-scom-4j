package com.enel.replicacomponente.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.scom.MedBarcodeSCOM;

public interface MedBarcodeSCOMRepository extends JpaRepository<MedBarcodeSCOM, Long> {

	MedBarcodeSCOM findByIdComponente(Long idComponente);

	Integer countByIdBarcode(Long idBarcode);
	
}
