package com.enel.replicacomponente.repository.sc4j;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.entity.sc4j.MedComponenteSC4J;

public interface MedComponenteSC4JRepository extends JpaRepository<MedComponenteSC4J, Long> {

	@Query(value = 
    		"  SELECT * FROM MED_COMPONENTE WHERE ID_COMPONENTE IN (select distinct(id_componente) from med_his_componente where 1=1\r\n"
    		+ "and (fec_hasta >= to_date(?1, 'dd/mm/yyyy') and fec_hasta < to_date(?2, 'dd/mm/yyyy'))\r\n"
    		+ "OR (fec_desde >= to_date(?1, 'dd/mm/yyyy') and fec_desde < to_date(?2, 'dd/mm/yyyy')))"
    	    , nativeQuery = true)
	List<MedComponenteSC4J> obtenerUltimosRegistros(String fechaDesde, String fechaHasta);

	@Query(value = 
    		"SELECT *\r\n"
    		+ "FROM med_componente mc, med_marca mma, med_modelo mmo\r\n"
    		+ "where mc.id_modelo = mmo.id_modelo\r\n"
    		+ "and mmo.id_marca = mma.id_marca\r\n"
    		+ "and mc.nro_componente = ?1\r\n"
    		+ "and mmo.cod_modelo = ?2\r\n"
    		+ "and mma.cod_marca = ?3"
    	    , nativeQuery = true)
	MedComponenteSC4J obtenerRegistro(String nroComponente, String modelo, String marca);

	@Query(value = 
    		"select distinct mc.* \r\n"
    		+ "from med_his_componente mhc, \r\n"
    		+ "     med_componente mc \r\n"
    		+ "where mhc.id_componente = mc.id_componente \r\n"
    		+ "and mhc.id_his_componente > ?1\r\n"
    		+ "and mhc.id_his_componente < ?2 "
    	    , nativeQuery = true)
	List<MedComponenteSC4J> obtenerUltimosRegistros4J(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal);

	@Query(value = 
    		"select max(mhc.ID_HIS_COMPONENTE)\r\n"
    		+ "from med_his_componente mhc, \r\n"
    		+ "     med_componente mc \r\n"
    		+ "where mhc.id_componente = mc.id_componente \r\n"
    		+ "and mhc.id_his_componente > ?1\r\n"
    		+ "and mhc.id_his_componente < ?2"
    	    , nativeQuery = true)
	Long obtenerIdMaxHisComponenteFinal(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal);

	

}
