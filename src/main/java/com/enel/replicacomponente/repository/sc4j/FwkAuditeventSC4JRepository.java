package com.enel.replicacomponente.repository.sc4j;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.sc4j.FwkAuditeventSC4J;

public interface FwkAuditeventSC4JRepository extends JpaRepository<FwkAuditeventSC4J, Long> {

	List<FwkAuditeventSC4J> findByIdFk(Long idComponente);

	Integer countByIdAuditevent(Long idAuditevent);

}
