package com.enel.replicacomponente.repository.sc4j;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.replicacomponente.entity.sc4j.MedFacMedModSC4J;
import com.enel.replicacomponente.entity.scom.MedFacMedModSCOM;

@Repository
public interface MedFacMedModSC4JRepository extends JpaRepository<MedFacMedModSC4J, Long> {

	List<MedFacMedModSC4J> findByIdMedidaModelo(Long idMedidaModelo);

	@Query(value = "select count(id_factor) "
			+ "from med_fac_med_mod where id_medida_modelo = ? group by id_medida_modelo", nativeQuery = true)
	Integer obtenerCountFactor(Long idMedidaModelo);

}
