package com.enel.replicacomponente.repository.sc4j;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.sc4j.MedCalibracionSC4J;

public interface MedCalibracionSC4JRepository extends JpaRepository<MedCalibracionSC4J, Long>{

	List<MedCalibracionSC4J> findByIdComponente(Long idComponente);

	Integer countByIdCalibracion(Long idCalibracion);

}
