package com.enel.replicacomponente.repository.sc4j;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.sc4j.DyoObjectSC4J;

public interface DyoObjectSC4JRepository extends JpaRepository<DyoObjectSC4J, Long> {

}
