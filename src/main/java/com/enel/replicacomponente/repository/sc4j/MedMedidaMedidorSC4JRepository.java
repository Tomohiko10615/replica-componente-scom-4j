package com.enel.replicacomponente.repository.sc4j;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.entity.sc4j.MedMedidaMedidorSC4J;

public interface MedMedidaMedidorSC4JRepository extends JpaRepository<MedMedidaMedidorSC4J, Long>{

	List<MedMedidaMedidorSC4J> findByIdComponente(Long idComponente);

	int countByIdComponente(Long idComponente);

	@Modifying
	@Transactional
	@Query(value = "delete from MED_MEDIDA_MEDIDOR where id_componente = ?1", nativeQuery = true)
	void deleteByIdComponente(Long idComponente);


}
