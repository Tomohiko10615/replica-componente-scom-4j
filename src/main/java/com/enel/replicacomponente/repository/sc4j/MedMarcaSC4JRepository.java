package com.enel.replicacomponente.repository.sc4j;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.sc4j.MedMarcaSC4J;

public interface MedMarcaSC4JRepository extends JpaRepository<MedMarcaSC4J, Long> {

}
