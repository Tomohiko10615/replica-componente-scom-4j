package com.enel.replicacomponente.repository.sc4j;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.enel.replicacomponente.entity.sc4j.MedMedidaModeloSC4J;

public interface MedMedidaModeloSC4JRepository extends JpaRepository<MedMedidaModeloSC4J, Long>{

	List<MedMedidaModeloSC4J> findByIdModelo(Long IdModelo);
	
	@Query(value = 
    		"select max(id_medida_modelo) from med_medida_modelo "
    	    , nativeQuery = true)
	Long obtenerIdMax();
	
	@Query(value = 
    		"select count(1) from med_fac_med_mod "
    	    , nativeQuery = true)
	Long obtenerTotalFac();

	@Query(value = 
    		"select distinct(id_medida_modelo) from med_medida_modelo where id_modelo = ?1 and id_medida = ?2 "
    	    , nativeQuery = true)
	Set<Long> verificarExistenciaMedidaModelo(Long idModelo, Long idMedida);
}
