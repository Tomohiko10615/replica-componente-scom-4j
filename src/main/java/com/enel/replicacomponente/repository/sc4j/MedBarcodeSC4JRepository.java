package com.enel.replicacomponente.repository.sc4j;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.sc4j.MedBarcodeSC4J;

public interface MedBarcodeSC4JRepository extends JpaRepository<MedBarcodeSC4J, Long> {

	Optional<MedBarcodeSC4J> findByIdComponente(Long idComponente);

	Integer countByIdBarcode(Long idBarcode);

}
