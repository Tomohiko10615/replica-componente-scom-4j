package com.enel.replicacomponente.repository.sc4j;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.replicacomponente.entity.sc4j.MedHisComponenteSC4J;

public interface MedHisComponenteSC4JRepository extends JpaRepository<MedHisComponenteSC4J, Long>{

	List<MedHisComponenteSC4J> findByIdComponente(Long idComponente);

	/*
	@Query(value = 
    		"select *\r\n"
    		+ "from med_his_componente mhc where 1=1 \r\n"
    		+ "and (fec_hasta > to_date(?1, 'dd/MM/yyyy hh24:mi:ss') and fec_hasta < sysdate) or \r\n"
    		+ "(fec_desde > to_date(?1, 'dd/MM/yyyy hh24:mi:ss') and fec_desde < sysdate)"
    	    , nativeQuery = true)
	List<MedHisComponenteSC4J> obtenerHistorial(String fechaDesde);
	*/

	@Query(value = 
    		"select *\r\n"
    		+ "from med_his_componente mhc where 1=1 \r\n"
    		+ "and id_componente = ?1"
    	    , nativeQuery = true)
	List<MedHisComponenteSC4J> obtenerRegistros(Long idComponente);

	Integer countByIdHisComponente(Long idHisComponente);

}
