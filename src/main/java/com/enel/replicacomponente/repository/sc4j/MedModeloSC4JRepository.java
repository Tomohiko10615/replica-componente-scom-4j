package com.enel.replicacomponente.repository.sc4j;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enel.replicacomponente.entity.sc4j.MedModeloSC4J;

public interface MedModeloSC4JRepository extends JpaRepository<MedModeloSC4J, Long> {

}
