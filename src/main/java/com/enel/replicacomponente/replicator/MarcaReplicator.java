package com.enel.replicacomponente.replicator;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.enel.replicacomponente.entity.sc4j.MedMarcaSC4J;
import com.enel.replicacomponente.entity.scom.MedMarcaSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.service.MarcaService;

@Component
public class MarcaReplicator {

	@Autowired
	private MarcaService marcaService;

	public void replicar(String fechaDesde, String fechaHasta) throws IOException {
		List<MedMarcaSCOM> medMarcaSCOMList =
				marcaService.obtenerUltimosRegistros(fechaDesde, fechaHasta);
		for (MedMarcaSCOM medMarcaSCOM : medMarcaSCOMList) {
			marcaService.verificarExistenciaEnSC4J(medMarcaSCOM.getId());
			MedMarcaSC4J registroReplicado = replicarRegistro(medMarcaSCOM);
			marcaService.replicarEnSC4J(registroReplicado);
		}
	}

	private MedMarcaSC4J replicarRegistro(MedMarcaSCOM medMarcaSCOM) {
		MedMarcaSC4J nuevoMedMarcaSC4J = new MedMarcaSC4J();
		BeanUtils.copyProperties(medMarcaSCOM, nuevoMedMarcaSC4J);
		nuevoMedMarcaSC4J.setIdMarca(medMarcaSCOM.getId());
		nuevoMedMarcaSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
		return nuevoMedMarcaSC4J;
	}
}
