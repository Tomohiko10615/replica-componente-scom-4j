package com.enel.replicacomponente.replicator;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.enel.replicacomponente.entity.sc4j.MedModeloSC4J;
import com.enel.replicacomponente.entity.scom.MedModeloSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.service.ModeloService;

@Component
public class ModeloReplicator {

	@Autowired
	private ModeloService modeloService;
	
	public void replicar(String fechaDesde, String fechaHasta) throws IOException {
		List<MedModeloSCOM> medModeloSCOMList = modeloService.obtenerUltimosRegistros(fechaDesde, fechaHasta);
		for (MedModeloSCOM medModeloSCOM : medModeloSCOMList) {
			modeloService.verificarExistenciaEnSC4J(medModeloSCOM.getId());
			MedModeloSC4J registroReplicado = replicarRegistro(medModeloSCOM);
			modeloService.replicarEnSC4J(registroReplicado);
		}
	}

	private MedModeloSC4J replicarRegistro(MedModeloSCOM medModeloSCOM) {
		MedModeloSC4J nuevoMedModeloSC4J = new MedModeloSC4J();
		BeanUtils.copyProperties(medModeloSCOM, nuevoMedModeloSC4J);
		nuevoMedModeloSC4J.setIdModelo(medModeloSCOM.getId());
		nuevoMedModeloSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
		return nuevoMedModeloSC4J;
	}

}
