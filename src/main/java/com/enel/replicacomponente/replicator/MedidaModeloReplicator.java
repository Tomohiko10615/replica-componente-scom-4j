package com.enel.replicacomponente.replicator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.enel.replicacomponente.dto.MedidaModeloFactorDTO;
import com.enel.replicacomponente.entity.sc4j.MedFacMedModSC4J;
import com.enel.replicacomponente.entity.sc4j.MedMedidaMedidorSC4J;
import com.enel.replicacomponente.entity.sc4j.MedMedidaModeloSC4J;
import com.enel.replicacomponente.entity.scom.MedFacMedModSCOM;
import com.enel.replicacomponente.entity.scom.MedMedidaModeloSCOM;
import com.enel.replicacomponente.helper.LogWriter;
import com.enel.replicacomponente.service.impl.MedidaModeloService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MedidaModeloReplicator {

	@Autowired
	private MedidaModeloService medidaModeloService;

	public void replicar() throws IOException {
		Long idMax = medidaModeloService.obtenerUltimosRegistro();
		List<MedMedidaModeloSCOM> medMedidaModeloSCOMList = medidaModeloService.obtenerUltimosRegistros(idMax);
		List<MedMedidaModeloSC4J> medMedidaModeloSC4JList = medidaModeloService.replicarRegistros(medMedidaModeloSCOMList);
		medidaModeloService.replicarEnSC4J(medMedidaModeloSC4JList);
		List<MedFacMedModSCOM> registrosPorReplicarFacMedMod = medidaModeloService.obtenerRegistros(medMedidaModeloSC4JList);
		List<MedFacMedModSC4J> medFacMedModSC4JList = medidaModeloService.replicarRegistrosFacMedMod(registrosPorReplicarFacMedMod);
		medidaModeloService.replicarEnSC4JFacMedMod(medFacMedModSC4JList);
		Long total4J = medidaModeloService.obtenerTotal4J();
		Long totalScom = medidaModeloService.obtenerTotalScom();
		//totalScom = total4J + 1;
		if (total4J < totalScom) {
			LogWriter.escribirLog("Existen registro faltantes en el SC4J");
			List<MedidaModeloFactorDTO> medidaModeloFactorDTOList = medidaModeloService.obtenerMedidaModeloFactor();
			List<Long> medModIdList = new ArrayList<>();
			Integer countFactorSC4J = null;
			for (MedidaModeloFactorDTO medidaModeloFactorDTO : medidaModeloFactorDTOList) {
				countFactorSC4J = medidaModeloService.obtenerCountFactorSC4J(medidaModeloFactorDTO.getIdMedidaModelo());
				log.info("countFactorSC4J: {}", countFactorSC4J);
				if (countFactorSC4J < medidaModeloFactorDTO.getCountIdFactor()) {
					medModIdList.add(medidaModeloFactorDTO.getIdMedidaModelo());
				}
			}
			registrosPorReplicarFacMedMod = medidaModeloService.obtenerRegistrosAdicionales(medModIdList);
			medFacMedModSC4JList = medidaModeloService.replicarRegistrosFacMedMod(registrosPorReplicarFacMedMod);
			medidaModeloService.replicarEnSC4JFacMedMod(medFacMedModSC4JList);
		}
		/*
		if (total4J < totalScom) {
			LogWriter.escribirLog("INCONGRUENCIA, la tabla med_fac_med_mod tiene más registros en 4J que en SCOM");
		} else {
			LogWriter.escribirLog("La tabla med_fac_med_mod esta actualizada");
		}
		*/
		
	}

}
