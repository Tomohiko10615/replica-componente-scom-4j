package com.enel.replicacomponente.replicator;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.enel.replicacomponente.entity.sc4j.MedComponenteSC4J;
import com.enel.replicacomponente.entity.sc4j.MedHisComponenteSC4J;
import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;
import com.enel.replicacomponente.entity.scom.MedHisComponenteSCOM;
import com.enel.replicacomponente.repository.scom.ComParametrosRepository;
import com.enel.replicacomponente.service.ComponenteService;
import com.enel.replicacomponente.service.impl.AuditeventService;
import com.enel.replicacomponente.service.impl.AuditeventService4J;
import com.enel.replicacomponente.service.impl.BarcodeService4J;
import com.enel.replicacomponente.service.impl.CalibracionService4J;
import com.enel.replicacomponente.service.impl.DyoService4J;
import com.enel.replicacomponente.service.impl.HisComponenteService;
import com.enel.replicacomponente.service.impl.HisComponenteService4J;
import com.enel.replicacomponente.service.impl.MedidaService4J;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Componente4JReplicator {

	@Autowired
	private ComponenteService componenteService;
	
	@Autowired
	private DyoService4J dyoService4J;
	@Autowired
	private MedidaService4J medidaService4J;
	@Autowired
	private BarcodeService4J barcodeService4J;
	@Autowired
	private AuditeventService4J auditeventService4J;
	@Autowired
	private HisComponenteService4J hisComponenteService4J;
	@Autowired
	private CalibracionService4J calibracionService4J;
	
	public Long replicar(Long idMaxHisComponenteInicial, Long idMaxHisComponenteFinal) throws IOException {
		List<MedComponenteSC4J> medComponenteSC4JList =
				componenteService.obtenerUltimosRegistros4J(idMaxHisComponenteInicial, idMaxHisComponenteFinal);
		log.info("Size medComponenteSC4JList: {}", medComponenteSC4JList.size());
		log.info("medComponenteSC4JList: {}", medComponenteSC4JList.toString());
		for (MedComponenteSC4J medComponenteSC4J : medComponenteSC4JList) {
			if (medComponenteSC4J.getIdDynamicobject() != null) {
				dyoService4J.replicarRegistroAsociado(medComponenteSC4J.getIdDynamicobject());
			}
			MedComponenteSCOM registroReplicado = replicarRegistro(medComponenteSC4J);
			componenteService.replicarEnSCOM(registroReplicado);
			replicarRegistrosAsociados(medComponenteSC4J);
		}
		hisComponenteService4J.replicarRegistroAsociado(medComponenteSC4JList);
		return componenteService.obtenerIdMaxHisComponenteFinal(idMaxHisComponenteInicial, idMaxHisComponenteFinal);
	}
	
	public void replicar(int limit) throws IOException {
		List<MedComponenteSC4J> medComponenteSC4JList =
				componenteService.obtenerUltimosRegistros4J(limit);
		log.info("Size medComponenteSC4JList: {}", medComponenteSC4JList.size());
		log.info("medComponenteSC4JList: {}", medComponenteSC4JList.toString());
		for (MedComponenteSC4J medComponenteSC4J : medComponenteSC4JList) {
			if (medComponenteSC4J.getIdDynamicobject() != null) {
				dyoService4J.replicarRegistroAsociado(medComponenteSC4J.getIdDynamicobject());
			}
			MedComponenteSCOM registroReplicado = replicarRegistro(medComponenteSC4J);
			componenteService.replicarEnSCOM(registroReplicado);
			replicarRegistrosAsociados(medComponenteSC4J);
		}
		hisComponenteService4J.replicarRegistroAsociado(medComponenteSC4JList);
	}

	private MedComponenteSCOM replicarRegistro(MedComponenteSC4J medComponenteSC4J) {
		MedComponenteSCOM nuevoMedComponenteSCOM = new MedComponenteSCOM();
		BeanUtils.copyProperties(medComponenteSC4J, nuevoMedComponenteSCOM);
		nuevoMedComponenteSCOM.setId(medComponenteSC4J.getIdComponente());
		return nuevoMedComponenteSCOM;
	}
	
	private void replicarRegistrosAsociados(MedComponenteSC4J medComponenteSC4J) throws IOException {
		medidaService4J.replicarRegistroAsociado(medComponenteSC4J.getIdComponente());
		barcodeService4J.replicarRegistroAsociado(medComponenteSC4J.getIdComponente());
		auditeventService4J.replicarRegistroAsociado(medComponenteSC4J.getIdComponente());
		calibracionService4J.replicarRegistroAsociado(medComponenteSC4J.getIdComponente());
	}

}
