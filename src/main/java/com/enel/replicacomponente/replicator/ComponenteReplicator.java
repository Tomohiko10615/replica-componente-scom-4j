package com.enel.replicacomponente.replicator;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.enel.replicacomponente.entity.sc4j.MedComponenteSC4J;
import com.enel.replicacomponente.entity.scom.MedComponenteSCOM;
import com.enel.replicacomponente.helper.GlobalConstants;
import com.enel.replicacomponente.service.ComponenteService;
import com.enel.replicacomponente.service.impl.AuditeventService;
import com.enel.replicacomponente.service.impl.BarcodeService;
import com.enel.replicacomponente.service.impl.CalibracionService;
import com.enel.replicacomponente.service.impl.DyoService;
import com.enel.replicacomponente.service.impl.HisComponenteService;
import com.enel.replicacomponente.service.impl.MedidaService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ComponenteReplicator {

	@Autowired
	private ComponenteService componenteService;
	
	@Autowired
	private DyoService dyoService;
	@Autowired
	private MedidaService medidaService;
	@Autowired
	private BarcodeService barcodeService;
	@Autowired
	private AuditeventService auditeventService;
	@Autowired
	private HisComponenteService hisComponenteService;
	@Autowired
	private CalibracionService calibracionService;
	
	public void replicar(String fechaDesde, String fechaHasta, List<MedComponenteSCOM> medComponenteSCOMList) throws IOException {
		for (MedComponenteSCOM medComponenteSCOM : medComponenteSCOMList) {
			//componenteService.verificarExistenciaEnSC4J(medComponenteSCOM.getId());
			if (medComponenteSCOM.getIdDynamicobject() != null) {
				dyoService.replicarRegistroAsociado(medComponenteSCOM.getIdDynamicobject());
			}
			MedComponenteSC4J registroReplicado = replicarRegistro(medComponenteSCOM);
			componenteService.replicarEnSC4J(registroReplicado);
			replicarRegistrosAsociados(medComponenteSCOM);
		}
		hisComponenteService.replicarRegistroAsociado(fechaDesde, fechaHasta, medComponenteSCOMList);
	}

	private MedComponenteSC4J replicarRegistro(MedComponenteSCOM medComponenteSCOM) {
		MedComponenteSC4J nuevoMedComponenteSC4J = new MedComponenteSC4J();
		BeanUtils.copyProperties(medComponenteSCOM, nuevoMedComponenteSC4J);
		nuevoMedComponenteSC4J.setIdComponente(medComponenteSCOM.getId());
		nuevoMedComponenteSC4J.setIdEmpresa(GlobalConstants.ID_ENEL);
		return nuevoMedComponenteSC4J;
	}
	
	private void replicarRegistrosAsociados(MedComponenteSCOM medComponenteSCOM) throws IOException {
		medidaService.replicarRegistroAsociado(medComponenteSCOM.getId());
		barcodeService.replicarRegistroAsociado(medComponenteSCOM.getId());
		auditeventService.replicarRegistroAsociado(medComponenteSCOM.getId());
		calibracionService.replicarRegistroAsociado(medComponenteSCOM.getId());
	}

}
