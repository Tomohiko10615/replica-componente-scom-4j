package com.enel.replicacomponente.dto;

public interface MedidorDTO {
	String getNroComponente();
	String getMarca();
	String getModelo();
}
