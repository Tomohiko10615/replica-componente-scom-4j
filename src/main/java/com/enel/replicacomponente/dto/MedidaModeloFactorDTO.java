package com.enel.replicacomponente.dto;

public interface MedidaModeloFactorDTO {
	Long getIdMedidaModelo();
	Integer getCountIdFactor();
}
